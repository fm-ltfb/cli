/*   pdev.c                                        F. Vernotte - 2015/05/06 */
/*   Computation of the Parabolic Deviation (PDev)                     */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sigma_theta_cli.h"

void usage(void)
/* Help message */
{
    printf("Usage: PDev SOURCE\n\n");
    printf(
        "Computes the Parabolic Deviations of a sequence of normalized "
        "frequency deviation measurements.\n\n");
    printf(
        "The input file SOURCE contains a 2-column table with time values "
        "(dates) in the first column and normalized frequency deviation "
        "measurements in the second column.\n\n");
    printf(
        "A 2-column table containing tau values (integration time) in the "
        "first column and Parabolic deviation measurement in the second "
        "column is sent to the standard output.\n\n");
    printf(
        "A redirection should be used for saving the results in a TARGET "
        "file: PDeV SOURCE > TARGET\n\n");
    printf(
        "SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de "
        "Franche-Comte/CNRS - FRANCE\n",
        st_version, st_date);
}

int main(int argc, char *argv[])
/* Compute PDEV serie from tau=tau0 (tau0=sampling step) to tau=N*tau0/2
   (N=number of samples) by octave (log step : tau_n+1=2*tau_n) */
/* Input : file name of the normalized frequency deviation samples*/
/* Output : tau \t PDev(tau) */
/*          (for tau=tau0 to tau=N*tau0/3 by octave) */
{
    int i, nbv, nto, tomax;
    ssize_t N;
    long int dtmx;
    char source[256], gm[100];
    FILE *ofd;
    double v1, v2, smpt, rslt;
    int err = 0;
    double tau = (double)1;
    st_serie serie;
    st_tau_inc ortau;
    flags_s flags;
    double *t = NULL;
    double *y = NULL;
    double *u = NULL;

    if (argc < 2) {
        usage();
        exit(-1);
    } else
        strcpy(source, *++argv);

    err = init_flag(&flags);
    if (err) return -2;
    flags.variance = PVAR;

    N = stio_load_ykt(source, &t, &y, &u, 1.0, 1.0, 0);
    if (N == -1)
        printf("# File not found\n");
    else {
        if (N < 2) {
            printf("# Unrecognized file\n\n");
            if (N == 1) printf("# Use 1col2col command\n\n");
            usage();
        } else {
            serie.tau_base = *(t + 1) - *(t);
            err = stu_populate_tau_list(&serie, N, flags.variance, OCT, NULL);
            if (err != 0) return -1;
            err = st_serie_dev(&serie, flags.variance, N, y);
            if (err != 0) return -1;

            for (i = 0; i < serie.length; ++i) {
                if (flags.debug)
                    printf("%.3e %.16e\n", serie.tau[i], serie.dev[i]);
                else
                    printf("%.3e %.2e\n", serie.tau[i], serie.dev[i]);
            }
        }
    }
}
