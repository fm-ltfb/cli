#include "sigma_theta.h"

static const char st_date[] = "01/01/1970";  // ST_DATE;  // define @build
static const char st_version[] = "0.0.1";   //ST_VERSION;  // define @build

#define MAXLINELENGTH 256
#define MAXCHAR 512

#define GPLDOPLOT (0x01)  // whether we actually run the gnuplot file or just build it
#define GPLPNG (0x02)  // whether we include png as an output format to the gnuplot file
#define GPLPDF (0x04)  // whether we include pdf as an output format to the gnuplot file
#define GPLX11 (0x08)  // whether we include X11 as an output (opens a graphic window whan
                       // running the gnuplot file
#define GPLDOKEY (0x10)  // whether we include a key in the plot with tau/adev pairs in txt

#define PATHSIZE (1024)
#define CONFIGFILE "SigmaTheta.conf"
// FIXME other OS :
#define CONFIGDIR "/usr/share/sigmatheta"

/**
 * \enum    flag_accred
 * \brief   Deviation type.
 * \details Used to specify a specific
 */
typedef enum {
    GNSS,      /*!< GNSS */
    CLOCKMIN,  /*!< Clock Minute */
    CLOCKHR    /*!< Clock Hour */
} flag_accred;

/**
 * \typedef  flag
 * \brief    Flags used in CLI apps.
 */
typedef struct flags_s {
    char graph;
    char conf;
    char bias;
    char title;
    char fit;
    char asymptote;
    char slopes[6];
    char display;
    char plot;
    int tau_pattern[10];  //TODO remove??
    char log_inc;
    int log_base;
    char variance;
    char forcetau;
    char accred_gnss;
    char accred_clockmin;
    char accred_clockhr;
    char debug;
} flags_s;



int init_flag(flags_s *flags);

int accred_tau_list(st_serie *serie, flags_s flags);

double scale(char code);

int gener_gplt(char *outfile, int N, double tau[], double adev[], st_conf_int conf_int[],
               char *est_typ, st_asymptote_coeff coeff, flags_s flags);

int gen_psdplt(char *outfile, int N, double freq[], double syf[],
               flags_s flags);

int gen_linplt(char *outfile, int N, double tt[], double xy[], int xory,
               flags_s flags);

int gen_gcodplt(char *outfile, char names[][MAXCHAR], int N, int nbf, double tau[],
                double gcod[][256], int ind_gcod, flags_s flags);

int gen_3chplt(char input[][MAXCHAR], char *outfile, int N, double tau[],
               double gcod[][256], double bmax[][256], int flagest,
               flags_s flags);