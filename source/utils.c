/*   utils.c                                        B. Dubois - 2024/03/14  */
/*   Utils subroutines                                                      */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "sigma_theta_cli.h"

#define db(x) ((double)(x))

#define GPLDOPLOT (0x01) // whether we actually run the gnuplot file or just build it
#define GPLPNG (0x02)    // whether we include png as an output format to the gnuplot file
#define GPLPDF (0x04)    // whether we include pdf as an output format to the gnuplot file
#define GPLX11 (0x08)    // whether we include X11 as an output (opens a graphic window whan running the gnuplot file
#define GPLDOKEY (0x10)  // whether we include a key in the plot with tau/adev pairs in txt

/**
 * \brief           Generate the real list of Tau.
 * \param *serie    Pointer to st_serie data structure.
 * \param n         Number of data samples
 * \param dev_type  Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param tau_inc_type Tau increment type (tau_increment_type -> int)
 * \param tau_base  Tau base value
 * \return          0 in case of succefull completion.
 */
int
accred_tau_list (st_serie *serie, flags_s flags)
{
    double tau_accred_gnss_list[6] = { 22.0, 45.0, 90.0, 180.0, 360.0, 720.0 };
    double tau_accred_clockmin_list[5] = { 360.0, 720.0, 1440.0, 2880.0, 5760.0 };
    double tau_accred_clockhr_list[5] = { 6.0, 12.0, 24.0, 48.0, 96.0 };

    if (flags.accred_gnss)
        {
            serie->length = 6;
            for (int i = 0; i < serie->length; i++)
                {
                    serie->tau[i] = tau_accred_gnss_list[i];
                }
            return 0;
        }
    if (flags.accred_clockmin)
        {
            serie->length = 5;
            for (int i = 0; i < serie->length; i++)
                {
                    serie->tau[i] = tau_accred_clockmin_list[i];
                }
            return 0;
        }
    if (flags.accred_clockhr)
        {
            serie->length = 5;
            for (int i = 0; i < serie->length; i++)
                {
                    serie->tau[i] = tau_accred_clockhr_list[i];
                }
            return 0;
        }
}

/**
 * \brief Returns a scaling factor as a double according to a character input.
 * \param code  Input character.
 * \return      A double value corresponding to input character.
 */
double
scale (char code)
{
    double factor;

    switch (code)
        {
        case 'd':
            //
            // d for days, 86400 s ; useful for timestamps in MJDs
            //
            factor = 86400.;
            break;
        case 'H':
        case 'h':
            //
            // H for hours, 3600 s ; useful for timestamps in hours
            //
            factor = 3600.;
            break;
        case 'M':
            //
            // M for minutes, 60 s ; useful for timestamps in minutes
            //
            factor = 60.;
            break;
        case 'm':
            //
            // m for milliseconds ; useful for data in milliseconds
            //
            factor = 1.e-3;
            break;
        case 'u':
            //
            // u for microseconds ; useful for data in microseconds
            //
            factor = 1.e-6;
            break;
        case 'n':
            //
            // n for nanoseconds ; useful for data in nanoseconds
            //
            factor = 1.e-9;
            break;
            //
        case 'p':
            //
            // p for picoseconds ; useful for data in picoseconds
            factor = 1.e-12;
            break;
        case 'f':
            //
            // f for femtoseconds ; useful for data in femtoseconds
            //
            factor = 1.e-15;
            break;
        case 'a':
            //
            // a for attoseconds ; useful for data in attoseconds. Why not ?..
            //
            factor = 1.e-18;
            break;
        default:
            // default: no scaling.
            //    No scaling is normally dealt with code = 0
            //    so no multiplication by 1 of a full dataset
            //    should ever occur under normal use.
            factor = 1.;
        }
    // fprintf(stderr,"# scale: factor %le input @%c@%d@\n", factor, code, code );
    return factor;
}

int
gener_key (FILE *ofd, char *infile)
{
    double tau[32], adev[32];
    float xbase, ybase, linewidth; // coordinates of the value labels
    int N, i;

    N = stio_load_adev (infile, tau, adev);

    if (N < 0)
        {
            fprintf (stderr, "gener_key: file %16s not found\n", infile);
            return -1;
        }

    if (N == 0)
        {
            fprintf (stderr, "gener_key: format of file %16s not recognized\n", infile);
            return -1;
        }

    if (ofd == NULL)
        {
            fprintf (stderr, "gener_key: output stream not opened or unavailable\n");
            return -1;
        }

    xbase = 0.8;
    ybase = 0.9;
    linewidth = 0.03;

    fprintf (ofd, "set key left\n");
    fprintf (ofd, "set label 1 \"%-14s %-12s\" at graph %.2f,%.2f \n", "Tau", "ADev", xbase, ybase);
    ybase -= linewidth;
    for (i = 0; i < N; ++i)
        {
            fprintf (ofd, "set label %d \"%6.2e   %6.2e\" at graph %.2f,%.2f \n", i + 2, tau[i], adev[i], xbase, ybase);
            ybase -= linewidth;
        }
}

/**
 * \brief  Generate a gnuplot file (.gnu) and invoke gnuplot for creating
 *         a postscript file
 */
int
gener_gplt (char *outfile, int N, double tau[], double adev[],
            st_conf_int conf_int[], char *est_typ, st_asymptote_coeff coeff,
            struct flags_s flags)
{
    int i, mii, mxi, err;
    double minx, maxx, miny, maxy, lmix, lmax, lmiy, lmay, ltx, lty, lmx, lmy,
        rtmx, rtmy;
    char gptfile[MAXCHAR], outviewfile[MAXCHAR], gpt_cmd[65536], sys_cmd[MAXCHAR];
    uint8_t doplot = 0;
    FILE *ofd;

    strncpy (gptfile, outfile, MAXCHAR - 1);
    strncat (gptfile, ".gnu", MAXCHAR - 1);
    strncpy (outviewfile, outfile, MAXCHAR - 1);

    ofd = fopen (gptfile, "w");
    if (ofd == NULL)
        return (-1);

    //
    // use a dummy terminal for initial plotting, the use replot to generate
    // requested output format(s)
    //
    fprintf (ofd, "\nset terminal dumb \n");
    fprintf (ofd, "set logscale xy\n");
    fprintf (ofd, "set format xy \"10^{%%+T}\"\n");
    fprintf (ofd, "set grid\n");
    minx = miny = 1e99;
    maxx = maxy = db (0);
    for (i = 0; i < N; ++i)
        {
            if (tau[i] < minx)
                minx = tau[i];
            if (tau[i] > maxx)
                maxx = tau[i];
            if (fabs (adev[i]) < miny)
                {
                    mii = i;
                    miny = fabs (adev[i]);
                }
            if (conf_int[i].bmax2s > maxy)
                {
                    mxi = i;
                    maxy = conf_int[i].bmax2s;
                }
        }

    if (flags.plot & GPLDOKEY)
        {
            // generates a key with all sampling time values
            gener_key (ofd, outfile);
        }
    else
        {
            if (mxi < mii)
                fprintf (ofd, "set key right\n");
            else
                fprintf (ofd, "set key left\n");
        }

    lmix = log (minx);
    lmax = log (maxx);
    lmiy = log (miny);
    lmay = log (maxy);
    ltx = db (0.05) * (lmax - lmix);
    lmix = lmix - ltx;
    lmax = lmax + ltx;
    lty = db (0.1) * (lmay - lmiy);
    lmiy = lmiy - lty;
    lmay = lmay + lty;
    lmx = lmax - db (0.15) * ltx;
    lmy = lmiy + db (0.25) * lty;
    rtmx = exp (lmx);
    rtmy = exp (lmy);
    minx = exp (lmix);
    maxx = exp (lmax);
    miny = exp (lmiy);
    maxy = exp (lmay);
    fprintf (ofd, "set xrange[%9.3e:%9.3e]\n", minx, maxx);
    fprintf (ofd, "set yrange[%9.3e:%9.3e]\n", miny, maxy);
    fprintf (ofd, "set mxtics 10\n");
    fprintf (ofd, "set mytics 10\n");
    if (flags.title)
        fprintf (ofd, "set title \"%s\" noenhanced\n", outfile);
    fprintf (ofd, "set xlabel \"Integration time τ [s]\"\n");
    fprintf (ofd, "set ylabel \"");
    switch (flags.variance)
        {
        case 1:
            fprintf (ofd, "M");
            break;

        case 2:
            fprintf (ofd, "H");
            break;

        case 3:
            fprintf (ofd, "P");
            break;

        case 5:
            fprintf (ofd, "H");
            break;

        default:
            fprintf (ofd, "A");
        }
    fprintf (ofd, "DEV σ_");
    switch (flags.variance)
        {
        case 1:
            fprintf (ofd, "M");
            break;

        case 2:
            fprintf (ofd, "H");
            break;

        case 3:
            fprintf (ofd, "P");
            break;

        case 5:
            fprintf (ofd, "H");
            break;

        default:
            fprintf (ofd, "A");
        }
    fprintf (ofd, "(τ)\"\n");
    if (flags.display || flags.plot & GPLPNG)
        {
            fprintf (ofd, "set style line 1 pt 2 ps 1 lc 7 lw 3\n");
            fprintf (ofd, "set style line 2 pt 2 ps 1 lc 7 lw 2\n");
            fprintf (ofd, "set style line 3 pt 6 ps 2 lc rgb \"#30D015\" lw 4\n");
            fprintf (ofd, "set style line 4 lc rgb \"#D01000\" lw 5\n");
            fprintf (ofd, "set style line 5 lc rgb \"#00A0A0\" lw 3\n");
            fprintf (ofd, "set style line 6 lc rgb \"#FFE000\" lw 3\n");
            fprintf (ofd, "set style line 7 lc rgb \"#109010\" lw 3\n");
            fprintf (ofd, "set style line 8 lc rgb \"#A000A0\" lw 3\n");
            fprintf (ofd, "set style line 9 lc rgb \"#0010D0\" lw 3\n");
            fprintf (ofd, "set style line 10 lc rgb \"#FF8000\" lw 3\n");
            fprintf (ofd, "set style line 11 pt 0 ps 1 lc 7 lw 3\n");
            fprintf (ofd, "set style line 12 pt 0 ps 1 lc rgb \"#A0A0A0\" lw 2\n");
            fprintf (ofd, "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font \"Verdana,6\"\n", st_version, rtmx, rtmy);
        }
    else
        {
            fprintf (ofd, "set style line 1 pt 2 ps 20 lc 7 lw 60\n");
            fprintf (ofd, "set style line 2 pt 2 ps 20 lc 7 lw 40\n");
            fprintf (ofd, "set style line 3 pt 6 ps 20 lc rgb \"#30D015\" lw 80\n");
            fprintf (ofd, "set style line 4 lc rgb \"#D01000\" lw 80\n");
            fprintf (ofd, "set style line 5 lc rgb \"#00C0FF\" lw 60\n");
            fprintf (ofd, "set style line 6 lc rgb \"#FFE000\" lw 60\n");
            fprintf (ofd, "set style line 7 lc rgb \"#109010\" lw 60\n");
            fprintf (ofd, "set style line 8 lc rgb \"#A000A0\" lw 60\n");
            fprintf (ofd, "set style line 9 lc rgb \"#0010D0\" lw 60\n");
            fprintf (ofd, "set style line 10 lc rgb \"#FF8000\" lw 60\n");
            fprintf (ofd, "set style line 11 pt 0 ps 20 lc 7 lw 60\n");
            fprintf (ofd, "set style line 12 pt 0 ps 20 lc rgb \"#A0A0A0\" lw 40\n");
            fprintf (ofd, "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font \"Verdana,8\"\n", st_version, rtmx, rtmy);
        }
    fprintf (ofd, "plot ");
    switch (flags.conf)
        {
        case 0:
            fprintf (ofd, "\"%s\" using 1:2 notitle with points ls 2 ", outfile);
            break;
        case 1:
            fprintf (ofd, "\"%s\" using 1:2 notitle with points ls 2, ",
                     outfile);
            fprintf (ofd,
                     "\"%s\" using 1:3:4:7 title \"95 %% confidence "
                     "interval\" with yerrorbars ls 12 ",
                     outfile);
            break;
        case 8:
            fprintf (ofd, "\"%s\" using 1:2 notitle with points ls 2, ",
                     outfile);
            fprintf (ofd,
                     "\"%s\" using 1:3:5:6 title \"68 %% confidence "
                     "interval\" with yerrorbars ls 11 ",
                     outfile);
            break;
        case 9:
            fprintf (ofd, "\"%s\" using 1:2 notitle with points ls 2, ",
                     outfile);
            fprintf (ofd,
                     "\"%s\" using 1:3:4:7 title \"95 %% confidence "
                     "interval\" with yerrorbars ls 12 ",
                     outfile);
            fprintf (ofd,
                     ", \"%s\" using 1:3:5:6 title \"68 %% confidence "
                     "interval\" with yerrorbars ls 11 ",
                     outfile);
            break;
        default:
            fprintf (ofd, "\"%s\" using 1:2 notitle with points ls 2, ",
                     outfile);
            fprintf (ofd,
                     "\"%s\" using 1:3:4:7 title \"95 %% confidence "
                     "interval\" with yerrorbars ls 12 ",
                     outfile);
        }
    if (flags.bias)
        fprintf (ofd,
                 ", \"%s\" using 1:3 title \"%s estimates\" with points ls 3",
                 outfile, est_typ);
    if (flags.fit)
        {
            fprintf (ofd, ", sqrt(%12.6e/x**3+", coeff[0]);
            fprintf (ofd, "%12.6e/x**2+", coeff[1]);
            fprintf (ofd, "%12.6e/x+", coeff[2]);
            fprintf (ofd, "%12.6e+", coeff[3]);
            fprintf (ofd, "%12.6e*x+", coeff[4]);
            fprintf (ofd, "%12.6e*x**2) notitle with line ls 4", coeff[5]);
        }
    if (flags.asymptote)
        {
            if (coeff[0] != 0)
                fprintf (ofd,
                         ", sqrt(%12.6e/x**3) title \"%7.1e τ^{-3/2}\" with line ls 5",
                         coeff[0], sqrt (coeff[0]));
            if (coeff[1] != 0)
                fprintf (ofd,
                         ", sqrt(%12.6e/x**2) title \"%7.1e τ^{-1}\" with line ls 6",
                         coeff[1], sqrt (coeff[1]));
            if (coeff[2] != 0)
                fprintf (ofd,
                         ", sqrt(%12.6e/x) title \"%7.1e τ^{-1/2}\" with line ls 7",
                         coeff[2], sqrt (coeff[2]));
            if (coeff[3] != 0)
                fprintf (ofd, ", sqrt(%12.6e) title \"%7.1e\" with line ls 8",
                         coeff[3], sqrt (coeff[3]));
            if (coeff[4] != 0)
                fprintf (ofd,
                         ", sqrt(%12.6e*x) title \"%7.1e τ^{1/2}\" with line ls 9",
                         coeff[4], sqrt (coeff[4]));
            if (coeff[5] != 0)
                fprintf (ofd,
                         ", sqrt(%12.6e*x**2) title \"%7.1e τ\" with line ls 10",
                         coeff[5], sqrt (coeff[5]));
        }

    //
    // if plotflag is EQUAL to 1, then let flags.display do the job (backward compatibility)
    //
    if (flags.plot == 1)
        {
            doplot = 1;
            if (flags.display)
                fprintf (ofd, "\nset terminal wxt size 1024,768 enhanced font \"Verdana\" fontscale 1.5 persist\nreplot\n");
            else
                {
                    fprintf (ofd, "\nset terminal pdfcairo size 172,128 enhanced color font \"Verdana\" fontscale 18\n");

                    // master
                    fprintf (ofd, "set output \"%s.pdf\"\n", outfile);
                    fprintf (ofd, "replot\n");
                    // ltfb branch : fprintf(ofd,"\nset output \"%s.pdf\"\nreplot\n",outfile);
                }
        }
    else
        {
            //
            // in any other case, lowercases doplot options will run the gnuplot file ;
            // upper cases and other options wont.
            //
            if (flags.plot & GPLDOPLOT)
                {
                    doplot = 1;
                }

            if (flags.plot & GPLPNG)
                {
                    fprintf (ofd, "\nset terminal png size 1440,1024 enhanced font \"Verdana\" fontscale 1.5\n");
                    fprintf (ofd, "set output \"%s.png\"\n", outfile);
                    fprintf (ofd, "replot\n");
                }

            if (flags.plot & GPLPDF)
                {
                    fprintf (ofd, "\nset terminal pdf size 1440,1024 enhanced font \"Verdana\" fontscale 1.5\n");
                    fprintf (ofd, "set output \"%s.pdf\"\n", outfile);
                    fprintf (ofd, "replot\n");
                }

            if (flags.plot & GPLX11)
                {
                    fprintf (ofd, "\nset terminal wxt size 1024,768 enhanced font \"Verdana\" fontscale 1.5 persist\n");
                    fprintf (ofd, "replot\n");
                }
        }

    fprintf (ofd, "\n");
    fprintf (ofd, "exit\n");
    fclose (ofd);

    if (doplot == 1)
        {
            strcpy (sys_cmd, "gnuplot ");
            strcat (sys_cmd, gptfile);
            fprintf (stderr, "# gener_gplt passing %s to system()\n", sys_cmd);
            err = system (sys_cmd);
        }

    return (err);
}

int
gen_psdplt (char *outfile, int N, double freq[], double syf[],
            struct flags_s flags)
/* Generate a gnuplot file (.gnu) and invoke gnuplot for creating a postscript
 * file */
{
    int i, mii, mxi, err;
    double minx, maxx, miny, maxy, lmix, lmax, lmiy, lmay, ltx, lty, lmx, lmy,
        rtmx, rtmy;
    char gptfile[256], psfile[256], sys_cmd[256];
    FILE *ofd;

    strcpy (gptfile, outfile);
    strcat (gptfile, ".gnu");
    strcpy (psfile, outfile);
    strcat (psfile, ".pdf");
    ofd = fopen (gptfile, "w");
    if (ofd == NULL)
        return (-1);
    //    fprintf(ofd,"set terminal postscript landscape enhanced color solid
    if (flags.display)
        fprintf (ofd, "\nset terminal wxt size 1024,768 enhanced font \"Verdana\" fontscale 1.5 persist\n");
    else
        {
            fprintf (ofd, "\nset terminal pdfcairo size 172,128 enhanced color font \"Verdana\" fontscale 18\n");
            fprintf (ofd, "set output \"%s\"\n", psfile);
        }
    fprintf (ofd, "set logscale xy\n");
    fprintf (ofd, "set format xy \"10^{%%+T}\"\n");
    fprintf (ofd, "set grid\n");
    minx = miny = 1e99;
    maxx = maxy = db (0);
    for (i = 0; i < N; ++i)
        {
            if (freq[i] < minx)
                minx = freq[i];
            if (freq[i] > maxx)
                maxx = freq[i];
            if (syf[i] < miny)
                {
                    mii = i;
                    miny = syf[i];
                }
            if (syf[i] > maxy)
                {
                    mxi = i;
                    maxy = syf[i];
                }
        }
    if (mxi < mii)
        fprintf (ofd, "set key right\n");
    else
        fprintf (ofd, "set key left\n");
    lmix = log (minx);
    lmax = log (maxx);
    lmiy = log (miny);
    lmay = log (maxy);
    ltx = db (0.05) * (lmax - lmix);
    lmix = lmix - ltx;
    lmax = lmax + ltx;
    lty = db (0.1) * (lmay - lmiy);
    lmiy = lmiy - lty;
    lmay = lmay + lty;
    lmx = lmax - db (0.15) * ltx;
    lmy = lmiy + db (0.25) * lty;
    rtmx = exp (lmx);
    rtmy = exp (lmy);
    minx = exp (lmix);
    maxx = exp (lmax);
    miny = exp (lmiy);
    maxy = exp (lmay);
    fprintf (ofd, "set xrange[%9.3e:%9.3e]\n", minx, maxx);
    fprintf (ofd, "set yrange[%9.3e:%9.3e]\n", miny, maxy);
    fprintf (ofd, "set mxtics 10\n");
    fprintf (ofd, "set mytics 10\n");
    if (flags.title)
        fprintf (ofd, "set title \"%s\" noenhanced\n", outfile);
    fprintf (ofd, "set xlabel \"Frequency f [Hz]\"\n");
    fprintf (ofd, "set ylabel \"PSD S_y(f)\"\n");
    fprintf (ofd, "set style line 1 pt 6 lc rgb \"#308015\" lw 3\n");
    fprintf (ofd, "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font \"Verdana,8\"\n", st_version, rtmx, rtmy);
    fprintf (ofd, "plot ");
    fprintf (ofd, "\"%s\" using 1:2 notitle with lines ls 1\n", outfile);
    fprintf (ofd, "exit\n");
    fclose (ofd);
    strcpy (sys_cmd, "gnuplot ");
    strcat (sys_cmd, gptfile);
    err = system (sys_cmd);
    return (err);
}

int
gen_linplt (char *outfile, int N, double tt[], double xy[], int xory,
            struct flags_s flags)
/* Generate a gnuplot file (.gnu) and invoke gnuplot for creating a postscript
 * file */
{
    int i, mii, mxi, err;
    double minx, maxx, miny, maxy, lmix, lmax, lmiy, lmay, ltx, lty, rtmx, rtmy;
    char gptfile[256], psfile[256], sys_cmd[256];
    FILE *ofd;

    strcpy (gptfile, outfile);
    strcat (gptfile, ".gnu");
    strcpy (psfile, outfile);
    strcat (psfile, ".pdf");
    ofd = fopen (gptfile, "w");
    if (ofd == NULL)
        return (-1);
    //    fprintf(ofd,"set terminal postscript landscape enhanced color solid
    //    \"Helvetica\" 18\n");
    if (flags.display)
        fprintf (ofd,
                 "set terminal wxt size 1024,768 enhanced font "
                 "\"Helvetica\" fontscale 1.5 persist\n");
    else
        {
            fprintf (ofd,
                     "set terminal pdfcairo size 172,128 enhanced color "
                     "font \"Helvetica\" fontscale 18\n");
            fprintf (ofd, "set output \"%s\"\n", psfile);
        }
    fprintf (ofd, "set format xy \"%%g\"\n");
    //    fprintf(ofd,"set format xy \"10^{%%+T}\"\n");
    fprintf (ofd, "set grid\n");
    minx = maxx = tt[0];
    miny = maxy = xy[0];
    for (i = 0; i < N; ++i)
        {
            if (tt[i] < minx)
                minx = tt[i];
            if (tt[i] > maxx)
                maxx = tt[i];
            if (xy[i] < miny)
                {
                    mii = i;
                    miny = xy[i];
                }
            if (xy[i] > maxy)
                {
                    mxi = i;
                    maxy = xy[i];
                }
        }
    if (mxi < mii)
        fprintf (ofd, "set key right\n");
    else
        fprintf (ofd, "set key left\n");
    ltx = db (0.05) * (maxx - minx);
    lmix = minx - ltx;
    lmax = maxx + ltx;
    lty = db (0.1) * (maxy - miny);
    lmiy = miny - lty;
    lmay = maxy + lty;
    minx = lmix;
    maxx = lmax;
    miny = lmiy;
    maxy = lmay;
    rtmx = lmax - db (0.15) * ltx;
    rtmy = lmiy + db (0.25) * lty;
    fprintf (ofd, "set xrange[%9.3e:%9.3e]\n", minx, maxx);
    fprintf (ofd, "set yrange[%9.3e:%9.3e]\n", miny, maxy);
    if (flags.title)
        fprintf (ofd, "set title \"%s\" noenhanced\n", outfile);
    fprintf (ofd, "set xlabel \"Time t [s]\"\n");
    fprintf (ofd, "set ylabel ");
    if (xory)
        fprintf (ofd, "\"Frequency deviation Y_k\"\n");
    else
        fprintf (ofd, "\"Time error x(t) [s]\"\n");
    fprintf (ofd, "set style line 1 pt 6 lc rgb ");
    if (xory)
        fprintf (ofd, "\"#D01000\"");
    else
        fprintf (ofd, "\"#0010D0\"");
    fprintf (ofd, " lw 3\n");
    fprintf (ofd,
             "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font "
             "\"Helvetica,8\"\n",
             st_version, rtmx, rtmy);
    fprintf (ofd, "plot ");
    fprintf (ofd, "\"%s\" using 1:2 notitle with lines ls 1\n", outfile);
    fprintf (ofd, "exit\n");
    fclose (ofd);
    strcpy (sys_cmd, "gnuplot ");
    strcat (sys_cmd, gptfile);
    err = system (sys_cmd);
    return (err);
}

/* Generate a gnuplot file (.gnu) and invoke gnuplot for creating a postscript
 * file */
int
gen_gcodplt (char *outfile, char names[][MAXCHAR], int N, int nbf,
             double tau[], double gcod[][256], int ind_gcod,
             struct flags_s flags)
{
    int i, j, err;
    // int mii,mxi;
    double minx, maxx, miny, maxy, lmix, lmax, lmiy, lmay, ltx, lty, lmx, lmy,
        rtmx, rtmy;
    char gptfile[MAXCHAR], psfile[MAXCHAR], sys_cmd[256];
    FILE *ofd;

    strcpy (gptfile, outfile);
    strcat (gptfile, ".gnu");
    strcpy (psfile, outfile);
    strcat (psfile, ".pdf");
    ofd = fopen (gptfile, "w");
    if (ofd == NULL)
        return (-1);

    if (flags.display)
        fprintf (ofd,
                 "set terminal wxt size 1024,768 enhanced font "
                 "\"Helvetica\" fontscale 1.5 persist\n");
    else
        {
            fprintf (ofd, "\nset terminal pdfcairo size 172,128 enhanced color font \"Verdana\" fontscale 18\n");
            fprintf (ofd, "set output \"%s\"\n", psfile);
        }
    fprintf (ofd, "set logscale xy\n");
    fprintf (ofd, "set format xy \"10^{%%+T}\"\n");
    fprintf (ofd, "set grid\n");
    minx = miny = 1e99;
    maxx = maxy = db (0);
    for (i = 0; i < N; ++i)
        {
            if (tau[i] < minx)
                minx = tau[i];
            if (tau[i] > maxx)
                maxx = tau[i];
        }
    for (j = 0; j < nbf; ++j)
        for (i = 0; i < N; ++i)
            {
                if (fabs (gcod[j][i]) < miny)
                    {
                        // mii=i;
                        miny = fabs (gcod[j][i]);
                    }
                if (fabs (gcod[j][i]) > maxy)
                    {
                        // mxi=i;
                        maxy = fabs (gcod[j][i]);
                    }
            }
    /*    if (mxi<mii)
          fprintf(ofd,"set key right\n");
          else*/
    fprintf (ofd, "set key left bottom reverse\n");
    lmix = log (minx);
    lmax = log (maxx);
    lmiy = log (miny);
    lmay = log (maxy);
    ltx = db (0.05) * (lmax - lmix);
    lmix = lmix - ltx;
    lmax = lmax + ltx;
    lty = db (0.1) * (lmay - lmiy);
    lmiy = lmiy - lty;
    lmay = lmay + lty;
    lmx = lmax - db (0.15) * ltx;
    lmy = lmiy + db (0.25) * lty;
    rtmx = exp (lmx);
    rtmy = exp (lmy);
    minx = exp (lmix);
    maxx = exp (lmax);
    miny = exp (lmiy);
    maxy = exp (lmay);
    fprintf (ofd, "set xrange[%9.3e:%9.3e]\n", minx, maxx);
    fprintf (ofd, "set yrange[%9.3e:%9.3e]\n", miny, maxy);
    fprintf (ofd, "set mxtics 10\n");
    fprintf (ofd, "set mytics 10\n");
    if (flags.title)
        fprintf (ofd, "set title \"%s\" noenhanced\n", outfile);
    fprintf (ofd, "set xlabel \"Integration time τ [s]\"\n");
    fprintf (ofd, "set ylabel \"ADEV σ_A(τ)\"\n");
    if (flags.display)
        {
            fprintf (ofd,
                     "set style line 1 lt 1 pt 4 ps 1.1 lc rgb \"#D01000\" lw 2\n");
            fprintf (ofd,
                     "set style line 2 lt 1 pt 12 ps 1.6 lc rgb "
                     "\"#308015\" lw 2\n");
            fprintf (ofd,
                     "set style line 3 lt 1 pt 6 ps 1.3 lc rgb \"#0010D0\" lw 2\n");
            fprintf (ofd,
                     "set style line 4 lt 1 pt 13 ps 1 lc rgb \"#806020\" lw 2\n");
            fprintf (ofd,
                     "set style line 5 lt 2 dt 2 pt 3 ps 1.2 lc rgb "
                     "\"#D01000\" lw 2\n");
            fprintf (ofd,
                     "set style line 6 lt 2 dt 2 pt 1 ps 1.4 lc rgb "
                     "\"#308015\" lw 2\n");
            fprintf (ofd,
                     "set style line 7 lt 2 dt 2 pt 2 ps 1.1 lc rgb "
                     "\"#0010D0\" lw 2\n");
            fprintf (ofd,
                     "set style line 8 lt 2 dt 2 pt 5 ps 1 lc rgb "
                     "\"#806020\" lw 2\n");
            fprintf (ofd,
                     "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font "
                     "\"Helvetica,6\"\n",
                     st_version, rtmx, rtmy);
        }
    else
        {
            fprintf (ofd,
                     "set style line 1 lt 1 pt 4 ps 22 lc rgb \"#D01000\" lw 60\n");
            fprintf (ofd,
                     "set style line 2 lt 1 pt 12 ps 32 lc rgb "
                     "\"#308015\" lw 60\n");
            fprintf (ofd,
                     "set style line 3 lt 1 pt 6 ps 26 lc rgb \"#0010D0\" lw 60\n");
            fprintf (ofd,
                     "set style line 4 lt 1 pt 13 ps 20 lc rgb "
                     "\"#806020\" lw 60\n");
            fprintf (ofd,
                     "set style line 5 lt 2 dt 2 pt 3 ps 24 lc rgb "
                     "\"#D01000\" lw 60\n");
            fprintf (ofd,
                     "set style line 6 lt 2 dt 2 pt 1 ps 28 lc rgb "
                     "\"#308015\" lw 60\n");
            fprintf (ofd,
                     "set style line 7 lt 2 dt 2 pt 2 ps 22 lc rgb "
                     "\"#0010D0\" lw 60\n");
            fprintf (ofd,
                     "set style line 8 lt 2 dt 2 pt 5 ps 20 lc rgb "
                     "\"#806020\" lw 60\n");
            fprintf (ofd,
                     "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font "
                     "\"Verdana,8\"\n",
                     st_version, rtmx, rtmy);
        }
    fprintf (ofd, "plot ");
    for (i = 0; i < nbf; ++i)
        {
            fprintf (ofd, "\"%s\" using 1:", &names[i][1]);
            if (names[i][0] == '-')
                fprintf (ofd, "(-$2)");
            else
                fprintf (ofd, "2");
            fprintf (ofd, " title \"%s\" with linespoints ls %d", names[i], i + 1);
            if (i < nbf - 1)
                fprintf (ofd, ",");
        }
    if (ind_gcod)
        {
            if (nbf == 4)
                --nbf;
            fprintf (ofd, ", ");
            for (i = 0; i < nbf; ++i)
                {
                    if (names[i][0] == '-')
                        names[i][0] = '+';
                    else
                        names[i][0] = '-';
                    fprintf (ofd, "\"%s\" using 1:", &names[i][1]);
                    if (names[i][0] == '-')
                        fprintf (ofd, "(-$2)");
                    else
                        fprintf (ofd, "2");
                    fprintf (ofd, " title \"%s\" with linespoints ls %d", names[i],
                             i + 5);
                    if (i < nbf - 1)
                        fprintf (ofd, ",");
                }
        }
    fprintf (ofd, "\n");
    fprintf (ofd, "exit\n");
    fclose (ofd);
    strcpy (sys_cmd, "gnuplot ");
    strcat (sys_cmd, gptfile);
    err = system (sys_cmd);
    return (err);
}

int
gen_3chplt (char input[][MAXCHAR], char *outfile, int N, double tau[],
            double gcod[][256], double bmax[][256], int flagest,
            struct flags_s flags)
/* Generate a gnuplot file (.gnu) and invoke gnuplot for creating a pdf file */
{
    int i, j, err, nbf = 4, no_cls = 0;
    // int mii,mxi;
    double minx, maxx, miny, maxy, lmix, lmax, lmiy, lmay, ltx, lty, lmx, lmy,
        rtmx, rtmy;
    char nomest[256], gptfile[MAXCHAR], psfile[MAXCHAR], sys_cmd[256];
    FILE *ofd;

    if (!input[3][0])
        no_cls = 1;
    strcpy (gptfile, outfile);
    strcat (gptfile, ".gnu");
    strcpy (psfile, outfile);
    strcat (psfile, ".pdf");
    ofd = fopen (gptfile, "w");
    if (ofd == NULL)
        {
            printf ("# File %s note created\n", gptfile);
            return (-1);
        }
    if (flags.display)
        fprintf (ofd,
                 "set terminal wxt size 1024,768 enhanced font "
                 "\"Verdana\" fontscale 1.5 persist\n");
    else
        {
            fprintf (ofd,
                     "set terminal pdfcairo size 172,128 enhanced color "
                     "font \"Verdana\" fontscale 18\n");
            fprintf (ofd, "set output \"%s\"\n", psfile);
            fprintf (ofd, "replot\n");
        }
    fprintf (ofd, "set logscale xy\n");
    fprintf (ofd, "set format xy \"10^{%%+T}\"\n");
    fprintf (ofd, "set grid\n");
    minx = miny = 1e99;
    maxx = maxy = db (0);
    for (i = 0; i < N; ++i)
        {
            if (tau[i] < minx)
                minx = tau[i];
            if (tau[i] > maxx)
                maxx = tau[i];
        }
    for (j = 0; j < nbf - 1; ++j)
        for (i = 0; i < N; ++i)
            {
                if (fabs (gcod[j][i]) < miny)
                    {
                        // mii=i;
                        miny = fabs (gcod[j][i]);
                    }
                if (fabs (bmax[j][i]) > maxy)
                    {
                        // mxi=i;
                        maxy = fabs (bmax[j][i]);
                    }
            }
    /*    if (mxi<mii)
          fprintf(ofd,"set key right\n");
          else*/
    fprintf (ofd, "set key left bottom Left reverse\n");
    lmix = log (minx);
    lmax = log (maxx);
    lmiy = log (miny);
    lmay = log (maxy);
    ltx = db (0.05) * (lmax - lmix);
    lmix = lmix - ltx;
    lmax = lmax + ltx;
    lty = db (0.1) * (lmay - lmiy);
    lmiy = lmiy - lty;
    lmay = lmay + lty;
    lmx = lmax - db (0.15) * ltx;
    lmy = lmiy + db (0.25) * lty;
    rtmx = exp (lmx);
    rtmy = exp (lmy);
    minx = exp (lmix);
    maxx = exp (lmax);
    miny = exp (lmiy);
    maxy = exp (lmay);
    fprintf (ofd, "set xrange[%9.3e:%9.3e]\n", minx, maxx);
    fprintf (ofd, "set yrange[%9.3e:%9.3e]\n", miny, maxy);
    fprintf (ofd, "set mxtics 10\n");
    fprintf (ofd, "set mytics 10\n");
    if (flags.title)
        fprintf (ofd, "set title \"%s\" noenhanced\n", outfile);
    fprintf (ofd, "set xlabel \"Integration time τ [s]\"\n");

    if ((flags.variance == HVAR) || (flags.variance == HCVAR))
        fprintf (ofd, "set ylabel \"HDEV σ_H(τ)\"\n");
    else
        fprintf (ofd, "set ylabel \"ADEV σ_A(τ)\"\n");
    fprintf (ofd, "set style fill transparent solid 0.25 border\n");
    if (flags.display)
        {
            fprintf (ofd,
                     "set style line 1 lt 1 pt 7 ps 1.5 lc rgb \"#FF0000\" lw 1\n");
            fprintf (ofd,
                     "set style line 2 lt 1 pt 7 ps 1.5 lc rgb \"#00FF00\" lw 1\n");
            fprintf (ofd,
                     "set style line 3 lt 1 pt 7 ps 1.5 lc rgb \"#0000FF\" lw 1\n");
            fprintf (ofd,
                     "set style line 11 lt 1 pt 7 ps 1.5 lc rgb "
                     "\"#FFA0A0\" lw 1\n");
            fprintf (ofd,
                     "set style line 12 lt 1 pt 7 ps 1.5 lc rgb "
                     "\"#A0FFA0\" lw 1\n");
            fprintf (ofd,
                     "set style line 13 lt 1 pt 7 ps 1.5 lc rgb "
                     "\"#A0A0FF\" lw 1\n");
            fprintf (ofd,
                     "set style line 21 lt 1 pt 4 ps 1.5 lc rgb "
                     "\"#D01000\" lw 2\n");
            fprintf (ofd,
                     "set style line 22 lt 1 pt 12 ps 1.5 lc rgb "
                     "\"#308015\" lw 2\n");
            fprintf (ofd,
                     "set style line 23 lt 1 pt 6 ps 1.5 lc rgb "
                     "\"#0010D0\" lw 2\n");
            fprintf (ofd,
                     "set style line 24 lt 1 pt 13 ps 1.5 lc rgb "
                     "\"#806020\" lw 2\n");
            fprintf (ofd,
                     "set style line 31 lt 2 dt 2 pt 3 ps 1.5 lc rgb "
                     "\"#D01000\" lw 2\n");
            fprintf (ofd,
                     "set style line 32 lt 2 dt 2 pt 1 ps 1.5 lc rgb "
                     "\"#308015\" lw 2\n");
            fprintf (ofd,
                     "set style line 33 lt 2 dt 2 pt 2 ps 1.5 lc rgb "
                     "\"#0010D0\" lw 2\n");
            fprintf (ofd,
                     "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font "
                     "\"Verdana,6\"\n",
                     st_version, rtmx, rtmy);
        }
    else
        {
            fprintf (ofd,
                     "set style line 1 lt 1 pt 7 ps 22 lc rgb \"#FF0000\" lw 20\n");
            fprintf (ofd,
                     "set style line 2 lt 1 pt 7 ps 22 lc rgb \"#00FF00\" lw 20\n");
            fprintf (ofd,
                     "set style line 3 lt 1 pt 7 ps 22 lc rgb \"#0000FF\" lw 20\n");
            fprintf (ofd,
                     "set style line 11 lt 1 pt 7 ps 22 lc rgb "
                     "\"#FFA0A0\" lw 20\n");
            fprintf (ofd,
                     "set style line 12 lt 1 pt 7 ps 22 lc rgb "
                     "\"#A0FFA0\" lw 20\n");
            fprintf (ofd,
                     "set style line 13 lt 1 pt 7 ps 22 lc rgb "
                     "\"#A0A0FF\" lw 20\n");
            fprintf (ofd,
                     "set style line 21 lt 1 pt 4 ps 22 lc rgb "
                     "\"#D01000\" lw 60\n");
            fprintf (ofd,
                     "set style line 22 lt 1 pt 12 ps 32 lc rgb "
                     "\"#308015\" lw 60\n");
            fprintf (ofd,
                     "set style line 23 lt 1 pt 6 ps 26 lc rgb "
                     "\"#0010D0\" lw 60\n");
            fprintf (ofd,
                     "set style line 24 lt 1 pt 13 ps 20 lc rgb "
                     "\"#806020\" lw 60\n");
            fprintf (ofd,
                     "set style line 31 lt 2 dt 2 pt 3 ps 24 lc rgb "
                     "\"#D01000\" lw 60\n");
            fprintf (ofd,
                     "set style line 32 lt 2 dt 2 pt 1 ps 28 lc rgb "
                     "\"#308015\" lw 60\n");
            fprintf (ofd,
                     "set style line 33 lt 2 dt 2 pt 2 ps 22 lc rgb "
                     "\"#0010D0\" lw 60\n");
            fprintf (ofd,
                     "set label \"SigmaTheta %s\" at %9.3e,%9.3e right font "
                     "\"Verdana,8\"\n",
                     st_version, rtmx, rtmy);
        }
    fprintf (ofd, "plot ");
    // 68% confidence interval
    for (i = 0; i < nbf - 1; ++i)
        {
            fprintf (ofd, "\"%s\" using 1:5:6", input[i]);
            fprintf (ofd, " title \"68%% conf. int. %d\" with filledcurves ls %d, ",
                     i + 1, i + 1);
        }
    // 95% confidence interval
    for (i = 0; i < nbf - 1; ++i)
        {
            fprintf (ofd, "\"%s\" using 1:4:7", input[i]);
            fprintf (ofd, " title \"95%% c. i. %d\" with filledcurves ls %d, ",
                     i + 1, i + 11);
        }
    // Direct estimates
    for (i = 0; i < nbf - 1; ++i)
        {
            fprintf (ofd, "\"%s\" using 1:2", input[i]);
            fprintf (ofd, " title \"direct estimates %d\" with linespoints ls %d, ",
                     i + 1, i + 21);
        }
    // Mean or median estimates
    switch (flagest)
        {
        case 0:
            strcpy (nomest, "mean");
            break;
        case 1:
            strcpy (nomest, "median");
            break;
        default:
            strcpy (nomest, "KLT");
        }
    for (i = 0; i < nbf - 2; ++i)
        {
            fprintf (ofd, "\"%s\" using 1:3", input[i]);
            fprintf (ofd, " title \"%s estimates %d\" with linespoints ls %d, ",
                     nomest, i + 1, i + 31);
        }
    fprintf (ofd, "\"%s\" using 1:3", input[nbf - 2]);
    fprintf (ofd, " title \"%s estimates %d\" with linespoints ls %d", nomest,
             nbf - 1, nbf + 29);
    // Closure
    if (no_cls)
        fprintf (ofd, "\n");
    else
        {
            fprintf (ofd, " ,\"%s\" using 1:2", input[3]);
            fprintf (ofd, " title \"measurement noise\" with linespoints ls %d\n",
                     24);
        }
    fprintf (ofd, "exit\n");
    fclose (ofd);
    strcpy (sys_cmd, "gnuplot ");
    strcat (sys_cmd, gptfile);
    err = system (sys_cmd);
    return (err);
}
