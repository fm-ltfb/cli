#include <ctype.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sigma_theta_cli.h"

#define db(x) ((double)(x))
#define sisig(x) ((x) == 0) ? (db (0)) : (((x) > 0) ? (db (1)) : (db (-1)))

/**
 *  \brief Help message
 */
void
usage (void)
{
    printf ("#################################################################"
            "#############################################\n\n");
    printf (" SigmaTheta : a tool from the SigmaTheta suite\n\n");
    printf ("    Usage : SigmaTheta [-amHph] [-x xscalingfactor] [SOURCE "
            "[TARGET]]\n\n");
    printf ("     Computes the (modified) Allan Deviation of a sequence of "
            "normalized frequency deviation measurements.\n\n");
    printf ("     If TARGET file is not specified, builds target name out of "
            "SOURCE name\n");
    printf ("     If SOURCE file is not specified, reads input from stdin and "
            "builds up a generic name for TARGET \n");
    printf ("     output goes to both stdout and the TARGET file\n\n");
    printf ("           -A use the standard Allan variance (default). \n");
    printf ("           -M use the modified Allan variance. \n");
    printf ("           -H use the Hadamard variance. \n");
    printf ("           -P use the Parabolic variance. \n");
    printf ("           -x xscalingfactor\n");
    printf ("                Units are SI units (s) by default ; should the "
            "input data be in other units (MJD, ns, ...)\n");
    printf (
        "                x option allows to properly normalize output : \n");
    printf ("                scaling factor is one of : \n");
    printf ("                    d : days  \n");
    printf ("                    H : hours  \n");
    printf ("                    M : minutes  \n");
    printf ("                    m : millisecond \n");
    printf ("                    u : microsecond \n");
    printf ("                    n : nanosecond \n");
    printf ("                    p : picosecond \n");
    printf ("                  A file containing data as MJD.XXXXX vs "
            "freq_dev can be processed with : \n");
    printf ("                  SigmaTheta -x d datafile\n\n");
    printf ("           -h : this message\n\n");
    printf ("    The input file SOURCE contains a 2-column table with time "
            "values (dates) in the first column\n");
    printf ("         and normalized frequency deviation measurements in the "
            "second column.\n\n");
    printf ("    A 7-column table is sent to the standard output and/or the "
            "output file with:\n");
    printf ("            1st column: tau values\n");
    printf ("            2nd column: (modified) Allan deviation estimate\n");
    printf ("            3rd column: unbiased estimate\n");
    printf ("            4th column: 2.5 %% bound\n");
    printf ("            5th column: 16 %% bound\n");
    printf ("            6th column: 84 %% bound\n");
    printf ("            7th column: 97.5 %% bound.\n\n");
    printf ("    The file TARGET.gnu is generated and used as an input to "
            "gnuplot.\n");
    printf ("    The configuration file \".SigmaTheta.conf\" is taken into "
            "account \n");
    printf ("        (e.g. selection of the modified Allan varance).\n\n");
    printf ("    The file TARGET.pdf is the pdf file of the gnuplot graph\n");
    printf ("        if the PDF option has been chosen in the configuration "
            "file.\n\n");
    printf ("SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de "
            "Franche-Comte/CNRS - FRANCE\n",
            st_version, st_date);
    printf ("#################################################################"
            "#############################################\n\n");
    exit (-1);
}

/**
 * main()
 */
int
main (int argc, char *argv[])
{
    int i, j, N, ineq, err;
    double tsas, asympt;
    double to2[256], edf[256]; //, ubad[256]; //, wght[256];
    double w[256]; // avar[256], adc[256], bmin[256], bmax[256], bi1s[256],
                   // bx1s[256],
    char scalex = 0, scaley = 0;
    char fsw = 0, fv = AVAR, command[32], source[MAXCHAR], outfile[MAXCHAR];
    char varchar = 'a', c;
    int index;
    size_t lensrc, stridx;
    st_serie serie;
    flags_s flags;
    conf_int rayl;
    double *t = NULL;
    double *y = NULL;
    FILE *ofd;

    opterr = 0;

    while ((c = getopt (argc, argv, "AMHPxh:")) != -1)
        switch (c)
            {
            case 'P':
                fsw = 1;
                fv = PVAR;
                varchar = c;
                break;
            case 'H':
                fsw = 1;
                fv = HVAR;
                varchar = c;
                break;
            case 'M':
                fsw = 1;
                fv = MVAR;
                varchar = c;
                break;
            case 'A':
                fsw = 1;
                fv = AVAR;
                varchar = c;
                break;
            case 'h':
                usage ();
                break;
            case 'x':
                scalex = optarg[0];
                break;
            case '?':
                if (optopt == 'x')
                    {
                        fprintf (stderr, "Option -%c requires an argument.\n",
                                 optopt);
                        usage ();
                    }
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr, "Unknown option character `\\x%x'.\n",
                             optopt);
                return 1;
            default:
                abort ();
            }

    index = optind;
    if (index < argc)
        {
            lensrc = strlen (strncpy (source, argv[index], MAXCHAR));
            printf ("# Input file %s\n", argv[index]);

            index++;
            if (index < argc)
                {
                    strncpy (outfile, argv[index], MAXCHAR);
                    printf ("# Output file %s\n", outfile);
                }
            else
                {
                    //
                    // SOURCE is specified but not TARGET ;
                    // lets build a reasonable output file name :
                    //
                    stridx = lensrc - 3;
                    if (source[stridx++] == 'y' && source[stridx++] == 'k'
                        && source[stridx++] == 't')
                        {
                            strncpy (outfile, source, MAXCHAR);
                            snprintf (outfile + lensrc - 3, 5, "%cdev",
                                      varchar);
                        }
                    else
                        {
                            snprintf (outfile, lensrc + 6, "%s.%cdev", source,
                                      varchar);
                        }
                    printf ("# Output file %s\n", outfile);
                }

            index++;
            if (index < argc)
                {
                    fprintf (stderr, "Extra parameter.\n");
                    usage ();
                }
        }
    else
        { // neither source nor target specified
            // build a generic filename, needed for gener_gplt
            snprintf (outfile, MAXCHAR, "st_generic_target.%cdev", varchar);
        }

    if (strlen (source) == 0)
        {
            fprintf (stderr, "#\n#\n# No input file given , expecting data on "
                             "stdin...\n#\n");
        }
    //------------------------------------------------------------------------
    err = 0;
    N = stio_load_ykt (source, &t, &y, NULL, scalex, scaley, 0);
    if (N == -1)
        {
            printf ("# File not found\n");
            return -1;
        }
    if (N < 2)
        {
            printf ("# Unrecognized file\n");
            if (N == 1)
                printf ("# Use 1col2col command\n\n");
            usage ();
            return -1;
        }
    err = init_flag (&flags);
    if (fsw)
        {
            flags.variance = fv;
            //        printf("flag_variance=%d\n",flag_variance);
        }
    if (flags.variance & 1)
        {
            flags.slopes[0] = 1;
            //        printf("flag_slopes[0]=%d\n",flag_slopes[0]);
        }
    else
        {
            flags.slopes[0] = 0;
            //        printf("flag_slopes[0]=%d\n",flag_slopes[0]);
        }
    if (flags.variance == PVAR)
        {
            flags.conf = 0;
        }
    /*        for (i=0;i<6;++i) printf("%d ",flag_slopes[i]);
            printf("\n");*/

    // Begin TMP
    flags.plot = 1;
    // End TMP

    //------------------------------------------------------------------------
    // for (i = 0; i < 256; ++i)
    //    wght[i] = 0;
    //------------------------------------------------------------------------

    serie.tau_base = *(t + 1) - *(t);

    err = stu_populate_tau_list (&serie, N, flags.variance, OCT, NULL);
    if (err != 0)
        return -1;

    err = st_serie_dev (&serie, flags.variance, N, y);
    if (err != 0)
        return -1;

    /*avar = malloc (serie.length * sizeof (avar));
    for (i = 0; i < serie.length; ++i)
        avar[i] = serie.dev[i] * serie.dev[i];
    */
    err = st_relatfit (&serie, serie.tau, 6, flags.slopes, 0); // flags.bias);
    for (i = 0; i < serie.length; ++i)
        {
            asympt = 0;
            if (flags.variance)
                {
                    for (j = 0; j < 5; ++j) /* The drift is not concerned */
                        {
                            tsas
                                = serie.asym[j] * st_interpo (serie.tau[i], j);
                            if (tsas > asympt)
                                {
                                    asympt = tsas;
                                    serie.alpha[i] = 2 - j;
                                }
                        }
                }
            else
                {
                    for (j = 1; j < 5; ++j) /* Neither the tau^-3/2 asymptote
                                               nor the drift are concerned */
                        {
                            tsas
                                = serie.asym[j] * st_interpo (serie.tau[i], j);
                            if (tsas > asympt)
                                {
                                    asympt = tsas;
                                    if (j == 1)
                                        serie.alpha[i] = +2;
                                    else
                                        serie.alpha[i] = 2 - j;
                                }
                        }
                }
        }
    st_avardof (&serie, edf, flags.variance);
    //------------------------------------------------------------------------
    if (flags.fit & 0x8)
        {
            for (i = 0; i < serie.length; ++i)
                w[i] = ((double)1) / edf[i];
            err = st_relatfit (&serie, w, 6, flags.slopes, 0); // flags.bias);

            for (i = 0; i < serie.length; ++i)
                {
                    asympt = 0;
                    if (flags.variance)
                        {
                            for (j = 0; j < 5;
                                 ++j) /* The drift is not concerned */
                                {
                                    tsas = serie.asym[j]
                                           * st_interpo (serie.tau[i], j);
                                    if (tsas > asympt)
                                        {
                                            asympt = tsas;
                                            serie.alpha[i] = 2 - j;
                                        }
                                }
                        }
                    else
                        {
                            for (j = 1; j < 5;
                                 ++j) /* Neither the tau^-3/2 asymptote nor the
                                         drift are concerned */
                                {
                                    tsas = serie.asym[j]
                                           * st_interpo (serie.tau[i], j);
                                    if (tsas > asympt)
                                        {
                                            asympt = tsas;
                                            if (j == 1)
                                                serie.alpha[i] = +2;
                                            else
                                                serie.alpha[i] = 2 - j;
                                        }
                                }
                        }
                }
            /*            for(i=0;i<N;++i) printf("%d \t ",alpha[i]);
                           printf("\n");*/
            st_avardof (&serie, edf, flags.variance);
            /*        printf("# Asymptote coefficients:\n");
                    printf("# tau^-1   \t tau^-1/2 \t tau^0    \t tau^1/2  \t
               tau^1\n"); for(i=0;i<5;++i) printf("%12.6e \t ",coeff[i]);
                    printf("\n");*/
        }
    //------------------------------------------------------------------------
    ofd = fopen (outfile, "w");
    switch (flags.variance)
        {
        case MVAR:
            printf ("# Tau       \t Mdev       \t Mdev unbiased\t 2.5 %% "
                    "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
            fprintf (ofd,
                     "# Tau       \t Mdev       \t Mdev unbiased\t 2.5 %% "
                     "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
            break;

        case HVAR:
            printf ("# Tau       \t Hdev       \t Hdev unbiased\t 2.5 %% "
                    "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
            fprintf (ofd,
                     "# Tau       \t Hdev       \t Hdev unbiased\t 2.5 %% "
                     "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
            break;

        case PVAR:
            printf ("# Tau       \t Pdev       \t Pdev unbiased\n");
            fprintf (ofd, "# Tau       \t Pdev       \t Pdev unbiased\n");
            break;

        default: // AVAR
            printf ("# Tau       \t Adev       \t Adev unbiased\t 2.5 %% "
                    "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
            fprintf (ofd,
                     "# Tau       \t Adev       \t Adev unbiased\t 2.5 %% "
                     "bound \t 16 %% bound \t 84 %% bound \t 97.5 %% bound\n");
        }
    for (i = 0; i < serie.length; ++i)
        {
            rayl = st_raylconfint (edf[i]);
            serie.conf_int[i].bmin2s
                = serie.dev[i] * sqrt (edf[i]) / rayl.sup_bound;
            serie.conf_int[i].bmax2s
                = serie.dev[i] * sqrt (edf[i]) / rayl.inf_bound;
            rayl = st_raylconfint1s (edf[i]);
            serie.conf_int[i].bmin1s
                = serie.dev[i] * sqrt (edf[i]) / rayl.sup_bound;
            serie.conf_int[i].bmax1s
                = serie.dev[i] * sqrt (edf[i]) / rayl.inf_bound;
            serie.dev_unb[i] = serie.dev[i] / rayl.mean;
            if (flags.variance != PVAR)
                {
                    printf ("%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e "
                            "\t %12.6e \t %12.6e\n",
                            serie.tau[i], serie.dev[i], serie.dev_unb[i],
                            serie.conf_int[i].bmin2s, serie.conf_int[i].bmin1s,
                            serie.conf_int[i].bmax1s,
                            serie.conf_int[i].bmax2s);
                    fprintf (
                        ofd,
                        "%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e \t "
                        "%12.6e \t %12.6e\n",
                        serie.tau[i], serie.dev[i], serie.dev_unb[i],
                        serie.conf_int[i].bmin2s, serie.conf_int[i].bmin1s,
                        serie.conf_int[i].bmax1s, serie.conf_int[i].bmax2s);
                }
            else
                {
                    printf ("%12.6e \t %12.6e \t %12.6e\n", serie.tau[i],
                            serie.dev[i], serie.dev_unb[i]);
                    fprintf (ofd, "%12.6e \t %12.6e \t %12.6e\n", serie.tau[i],
                             serie.dev[i], serie.dev_unb[i]);
                }
        }
    fclose (ofd);
    //------------------------------------------------------------------------
    for (i = 0; i < serie.length; ++i)
        {
            w[i] = db(1) / edf[i];
        }

    err = st_relatfit (&serie, w, 6, flags.slopes, flags.bias);
    if (err != 0)
        {
            printf ("Error computing asymptotes: %d\n", err);
            return -1;
        }
    printf ("# Asymptote coefficients:\n");
    printf ("# tau^-3/2 \t tau^-1   \t tau^-1/2 \t tau^0    \t tau^1/2  \t "
            "tau^1\n");
    for (i = 0; i < 6; ++i)
        {
            printf ("%12.6e \t ", serie.asym[i]);
        }
    printf ("\n");
    if (flags.graph)
        {
            /* Use of gnuplot for generating the graph as a ps file */
            err = gener_gplt (outfile, serie.length, serie.tau, serie.dev,
                              serie.conf_int, "unbiased", serie.asym, flags);
            if (err)
                printf ("# Error %d: ps file not created\n", err);
        }

    //------------------------------------------------------------------------
    free (t);
    free (y);

    return (0);
}
