/*   psdgraph.c                                   F. Vernotte - 2015/06/24  */
/*   Plot the Power Spectral Density of a .ykt file (normalized frequency   */
/*   deviation) versus the frequency                                        */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <gsl/gsl_fft_real.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sigma_theta_cli.h"

#define db(x) ((double)(x))
#define sisig(x) ((x) == 0) ? (db (0)) : (((x) > 0) ? (db (1)) : (db (-1)))

void
usage (void)
/* Help message */
{
    printf ("Usage: PSDGraph yktFILE\n\n");
    printf ("Plots the graph of the Power Spectrum Density (PSD) of normalized frequency deviation versus the frequency.\n\n");
    printf ("The input file yktFILE contains a 2-column table with dates (in s) in the first column and frequency deviation samples (yk) in the second column.\n\n");
    printf ("The PSD versus the frequency are stored in the output file yktFILE.psd.\n");
    printf ("The file yktFILE.psd.gnu is generated for invoking gnuplot.\n");
    printf ("The file yktFILE.psd.pdf is the pdf file of the gnuplot graph (if the PDF option has been chosen in the configuration file)\n\n");
    printf ("If the option '-d' is selected, the number of data is decimated to be limited to 10 000 samples\n\n");
    printf ("SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de Franche-Comte/CNRS - FRANCE\n", st_version, st_date);
}

int
main (int argc, char *argv[])
{
    char source[256], psdfile[256];
    int N, M, err, rep, i, j, nc, idec;
    double stride, l2n, tot_dur, ksy;
    char fv;
    flags_s flags;
    st_psd *psd;
    FILE *ofd;
    double *t = NULL;
    double *y = NULL;
    fv = 0;
    idec = 0;

    while ((rep = getopt (argc, argv, "d")) != -1)
        switch (rep)
            {
            case 'd':
                idec = 1;
                break;
            case '?':
                printf ("# Unknown option '-%c'\n", optopt);
                usage ();
            default:
                exit (-1);
            }
    
    if (argc - optind < 1)
        {
            printf ("# Missing arguments\n");
            usage ();
            exit (-1);
        }
    if (argc - optind > 1)
        {
            printf ("# Too many arguments\n");
            usage ();
            exit (-1);
        }
    strcpy (source, argv[optind]);

    err = init_flag (&flags);
    if (err == -1)
        printf ("# ~/.SigmaTheta.conf not found, default values selected\n");
    if (err == -2)
        printf ("# ~/.SigmaTheta.conf improper, default values selected\n");
    // flags.variance = fv; // Why set variance flag ???
    strcpy (psdfile, source);
    strcat (psdfile, ".psd");

    // TODO: handle scalex, scaley
    //N = stio_load_ykt (source, &t, &y, NULL, scalex, scaley, 0);
    N = stio_load_ykt (source, &t, &y, NULL, 1, 1, 0);
    if (N == -1)
        printf ("File %s not found\n", source);
    else
        {
            if (N < 2)
                {
                    printf ("%s: unrecognized file\n\n", source);
                    usage ();
                }
            else
                {
                    stride = t[1] - t[0];
                    psd = malloc(sizeof(st_psd));
                    err = st_psdgraph (psd, y, stride, N, idec);
                    if (err)
                        printf ("PSD computation error: %d\n", err);
                    ofd = fopen (psdfile, "w");
                    for (i = 0; i < psd->length; ++i)
                        {
                            fprintf (ofd, "%e %e\n", psd->ff[i], psd->syy[i]);
                        }
                    fclose (ofd);
                    /* Use of gnuplot for generating the graph as a ps file */
                    err = gen_psdplt (psdfile, psd->length, psd->ff, psd->syy, flags);
                    if (err)
                        printf ("ps file generation error: %d\n", err);
                    free(psd);
                }
        }
}
