/*   flags.c                                        B. Dubois - 2024/03/14  */
/*   Flag management subroutines                                            */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "sigma_theta_cli.h"

/**
 * \brief       Initialize flags by reading the configuration file
 *              $HOME/.SigmaTheta.conf
 * \param flag  Structure containing flags
 * \return      0 if method complete without error
 */
int
init_flag (flags_s *flags)
{
    char *homedir, filepath[PATHSIZE], gm[100], *fg, tg[100];
    int tfs, i, indpb, written_char, ntau;
    long deb_file;
    FILE *ofd;
    struct stat sb;

    homedir = getenv ("HOME");

    if (homedir == NULL)
        { // if no HOME environment, only <filename> with a
          // leading '.' goes to filepath
            written_char = snprintf (filepath, PATHSIZE, ".%s", CONFIGFILE);
        }
    else
        { // otherwise, full path
            written_char
                = snprintf (filepath, PATHSIZE, "%s/.%s", homedir, CONFIGFILE);
        }
    if (written_char >= PATHSIZE)
        {
            // if pathfile is saturated by the requested path (most likely, too
            // big HOME, then fallback to default config file with a warning
            fprintf (
                stderr,
                "# Error, string too long (%d char) while generating config "
                "filename\n",
                written_char);
            fprintf (stderr, "#   ENV HOME = %s\n", homedir);
            fprintf (
                stderr,
                "#   WARNING = default configuration file %s/%s will be used "
                "instead.\n",
                CONFIGDIR, CONFIGFILE);
            return -1;
        }

    if (stat (filepath, &sb) < 0) // default-ing to systemwide config file
        written_char
            = snprintf (filepath, PATHSIZE, "%s/%s", CONFIGDIR, CONFIGFILE);

    // if (homedir != NULL) {
    //     strcpy(filepath, homedir);
    //     strcat(filepath, file);
    // } else
    //     strcpy(filepath, filwin);

    // hardcoded config, should config file failed to open
    flags->conf = 1;      /* default value: 95%                   */
    flags->bias = 0;      /* default value: OFF                   */
    flags->graph = 1;     /* default value: ON                    */
    flags->title = 1;     /* default value: ON                    */
    flags->fit = 9;       /* default value: double fit            */
    flags->asymptote = 1; /* default value: ON                    */
    for (i = 0; i < 6; ++i)
        {
            flags->slopes[i] = 1; /* default value: fit all slopes    */
        }
    flags->variance = AVAR; /* default value: Allan variance        */
    flags->slopes[0] = 0;   /* No tau^-3/2 slope for Allan variance */
    flags->plot = 0;
    flags->tau_pattern[0] = 2;
    flags->log_inc = 1;

    if (written_char >= PATHSIZE)
        { // pathological case of too long config
          // file filename. Should never occur.
            fprintf (stderr,
                     "# Unexpected error, string too long (%d char) while "
                     "generating config filename\n",
                     written_char);
            fprintf (stderr, "#   ENV HOME = CONFIGDIR %s CONFIGFILE %s \n",
                     CONFIGDIR, CONFIGFILE);
            fprintf (stderr, "# using hardcoded defaults.\n");
            return -2;
        }

    ofd = fopen (filepath, "r");
    if (ofd == NULL)
        {
            fprintf (stderr, "# cannot open config file %s\n", filepath);
            written_char = snprintf (filepath, PATHSIZE, "%s/%s", CONFIGDIR,
                                     CONFIGFILE);
            fprintf (stderr, "# using hardcoded defaults.\n");
            return (-2);
        }
    else
        {
            do
                {
                    fg = fgets (gm, 100, ofd);
                    if (fg == NULL)
                        return (-2);
                }
            while (strcmp (fg, "# 1. Confidence Interval\n"));
            deb_file = ftell (ofd);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[12] = 0;
                }
            while (strcmp (tg, "95% bounds: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[12]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->conf = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[12] = 0;
                }
            while (strcmp (tg, "68% bounds: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[12]);
                    if (!strcmp (tg, "ON\n"))
                        flags->conf += 8;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[20] = 0;
                }
            while (strcmp (tg, "Unbiased estimates: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[20]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->bias = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[7] = 0;
                }
            while (strcmp (tg, "Graph: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[7]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->graph = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[7] = 0;
                }
            while (strcmp (tg, "Title: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[7]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->title = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[5] = 0;
                }
            while (strcmp (tg, "Fit: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[5]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->fit -= 1;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[12] = 0;
                }
            while (strcmp (tg, "Double Fit: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[12]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->fit -= 8;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            flags->asymptote = 1; /* default value : with asymptotes */
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[12] = 0;
                }
            while (strcmp (tg, "Asymptotes: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[12]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->asymptote = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[16] = 0;
                }
            while (strcmp (tg, "Tau^-3/2 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[16]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[0] = 0;
                    else
                        flags->slopes[0] = 1; /* set to 0 at initialization */
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[14] = 0;
                }
            while (strcmp (tg, "Tau^-1 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[14]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[1] = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[16] = 0;
                }
            while (strcmp (tg, "Tau^-1/2 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[16]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[2] = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[13] = 0;
                }
            while (strcmp (tg, "Tau^0 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[13]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[3] = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[15] = 0;
                }
            while (strcmp (tg, "Tau^1/2 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[15]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[4] = 0;
                }
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[13] = 0;
                }
            while (strcmp (tg, "Tau^1 slope: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[13]);
                    if (!strcmp (tg, "OFF\n"))
                        flags->slopes[5] = 0;
                }
            indpb = 0;
            for (i = 0; i < 6; ++i)
                indpb += flags->slopes[i];
            if (!indpb)
                for (i = 0; i < 6; ++i)
                    flags->slopes[i] = 1;
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[25] = 0;
                }
            while (strcmp (tg, "Modified Allan variance: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[25]);
                    if (!strcmp (tg, "ON\n"))
                        {
                            flags->variance = 1;
                        }
                    else
                        {
                            flags->slopes[0] = 0;
                        }
                }
            flags->log_inc = 1;
            flags->log_base = 2; /* default value: tau increment by octave */
            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[5] = 0;
                }
            while (strcmp (tg, "Tau: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[5]);
                    ntau = sscanf (tg, "%d %d %d %d %d %d %d %d %d %d",
                                   &flags->tau_pattern[0], &flags->tau_pattern[1],
                                   &flags->tau_pattern[2], &flags->tau_pattern[3],
                                   &flags->tau_pattern[4], &flags->tau_pattern[5],
                                   &flags->tau_pattern[6], &flags->tau_pattern[6],
                                   &flags->tau_pattern[8], &flags->tau_pattern[9]);
                    if (ntau > 0)
                        {
                            if (flags->tau_pattern[0] > 1)
                                flags->log_base = flags->tau_pattern[0];
                            else if ((flags->tau_pattern[0]==1) && (ntau>1))
                                flags->log_inc = 0;
                        }
                }
            flags->display = 1;
            tfs=fseek(ofd,deb_file,SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[8] = 0;
                }
            while (strcmp (tg, "Output: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[8]);
                    if (!strncmp (tg, "PDF", 3))
                        flags->display = 0;
                }

            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[20] = 0;
                }
            while (strcmp (tg, "Plot: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[20]);
                    if (!strncmp (tg, "ON", 2))
                        flags->plot = (char)1;
                }

            tfs = fseek (ofd, deb_file, SEEK_SET);
            do
                {
                    fg = fgets (gm, 100, ofd);
                    strcpy (tg, gm);
                    tg[7] = 0;
                }
            while (strcmp (tg, "Debug: ") && (fg));
            if (fg)
                {
                    strcpy (tg, &gm[7]);
                    if (!strncmp (tg, "ON", 2))
                        {
                            flags->debug = (char)1;
                        }
                }
            return (0);
        }
}
