/*   3_cornered_hat.c                          F. Vernotte - 2019/07/28     */
/*   Computation of the Groslambert covariances of a 3-cornered hat 	    */
/*   system. The confidence intervals are estimated by using the KLTG	    */
/*   and KLTS methods.							    */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sigma_theta_cli.h"

#define DATAMAX 10000000 /* Maximum sequence size */
#define NUMAX 1024       /* Maximum EDF numer */

/**
 * \brief  Help message
 */
void
usage (void)
{
    printf ("Usage: 3CorneredHat [-G|C|H] [-m|f|d|i] SOURCE12 SOURCE23 SOURCE32 TARGET\n\n");
    printf ("Computes the Allan deviation of each clock of a 3-cornered hat system and estimates the confidence intervals.\n\n");
    printf ("The input files SOURCEij contains a 2-column table with time values (dates) in the first column and normalized frequency deviation measurements of clock 'j' compared to clock 'i' in the second column. All input files must have the same dates.\n\n");
    printf ("The root TARGET is used to build the 3 output files for clocks 1, 2 and 3: TARGET1.gcod, TARGET2.gcod and TARGET3.gcod.\n\n");
    printf ("Each of these files is a 7-column table with:\n");
    printf ("\t1st column: tau values\n");
    printf ("\t2nd column: Groslambert codeviation estimate\n");
    printf ("\t3rd column: mean or 50 %% estimate\n");
    printf ("\t4th column: 2.5 %% bound\n");
    printf ("\t5th column: 16 %% bound\n");
    printf ("\t6th column: 84 %% bound\n");
    printf ("\t7th column: 97.5 %% bound.\n");
    printf ("The configuration file \".SigmaTheta.conf\" is taken into account (e.g. choice of the increment of the tau values).\n\n");
    printf ("The file TARGET.gnu is generated for invoking gnuplot. \n");
    printf ("The file TARGET.pdf is the pdf file of the gnuplot graph (if the PDF option has been chosen in the configuration file).\n\n");
    printf ("If the option '-G' is selected, the Groslambert covariance method is invoked (default). \n");
    printf ("If the option '-C' is selected, the classical 3-cornered hat method is invoked. \n");
    printf ("If the option '-H' is selected, the Hadamard covariance method is invoked.\n");
    printf ("If the option '-m' is selected, the mean estimate is displayed in the 3rd column of the output files (default).\n");
    printf ("If the option '-f' is selected, the 50 %% estimate (median) is displayed in the 3rd column of the output files.\n");
    printf ("If the option '-d' is selected, a drift is removed from all SOURCEij data.\n");
    printf ("If the option '-i' is selected, 3 individual graphs are displayed (one for each clock).\n\n");
    printf ("SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de Franche-Comte/CNRS - FRANCE\n", st_version, st_date);
}

int
main (int argc, char *argv[])
{
    char source1[MAXCHAR], source2[MAXCHAR], source3[MAXCHAR];
    char output[MAXCHAR], target[4][MAXCHAR], chestim[MAXCHAR];
    int err, rep, gflag, cflag, aflag, fflag, hflag, fv_pr, fv_alt;
    int dflag, iflag, ind3ch, ind50;
    int i, j, k, N, klt; // , alpha[256]
    long nmont;
    double measdev[256], varnoise[256];      // tau[256],
    double edfm[256], tsas, asympt, log_inc; // avar[256],
    double edf[3][256];
    double mesti[3], est[3][3], ci[3][5];
    double bmin[3][256], bmax[3][256];
    double *montecarl[3], *pmont[3], *asort[3], *cd, *expal;
    long *indices[3], *indix;
    FILE *tarf[3]; //, *tarf2, *tarf3;
    st_serie serie[3];
    st_serie gadev[4];
    flags_s flags;
    st_asymptote_coeff a_coeff;
    st_tchebyfit_coeff t_coeff;

    double *t = NULL;
    double *y = NULL;
    // double *y3 = NULL;
    double *y12 = NULL;
    double *y23 = NULL;
    double *y31 = NULL;

    flags.variance = AVAR;

    err = gflag = cflag = aflag = fflag = hflag = dflag = iflag = ind3ch = ind50 = 0;
    strcpy (chestim, "mean  ");
    while ((rep = getopt (argc, argv, "gcmfdi")) != -1)
        switch (rep)
            {
            case 'G':
                gflag = 1;
                break;
            case 'C':
                cflag = ind3ch = 1;
                break;
            case 'H':
                hflag = 1;
                break;
            case 'm':
                aflag = 1;
                break;
            case 'f':
                fflag = ind50 = 1;
                strcpy (chestim, "median");
                break;
            case 'd':
                dflag = 1;
                break;
            case 'i':
                iflag = 1;
                break;
            case '?':
                printf ("# Unknown option '-%c'\n", optopt);
                usage ();
            default:
                exit (-1);
            }
    if ((gflag & cflag) || (gflag & hflag) || (cflag & hflag) || (gflag & cflag & hflag))
        {
            printf ("# Incompatible options '-G' and '-C'\n");
            usage ();
            exit (-1);
        }
    if (aflag & fflag)
        {
            printf ("# Incompatible options '-m' and '-f'\n");
            usage ();
            exit (-1);
        }
    if (hflag)
        {
            fv_pr = flags.variance = HCVAR;
            fv_alt = 2;
        }
    else
        {
            fv_pr = flags.variance = GVAR;
            fv_alt = 0;
        }
    if (argc - optind < 4)
        {
            printf ("# Missing arguments\n");
            usage ();
            exit (-1);
        }
    if (argc - optind > 4)
        {
            printf ("# Too many arguments\n");
            usage ();
            exit (-1);
        }

    strcpy (source1, argv[optind]);
    strcpy (source2, argv[optind + 1]);
    strcpy (source3, argv[optind + 2]);
    strcpy (output, argv[optind + 3]);

    for (i = 0; i < 4; ++i)
        strcpy (target[i], output);
    if (ind3ch)
        {
            strcat (target[0], "_1.3ch");
            strcat (target[1], "_2.3ch");
            strcat (target[2], "_3.3ch");
        }
    else
        {
            strcat (target[0], "_1.gcod");
            strcat (target[1], "_2.gcod");
            strcat (target[2], "_3.gcod");
        }
    strcat (target[3], "_cls.adev");

    err = init_flag (&flags);
    if (err == -1)
        printf ("# ~/.SigmaTheta.conf not found, default values selected\n");
    if (err == -2)
        printf ("# ~/.SigmaTheta.conf improper, default values selected\n");
    if (err)
        {
            flags.log_inc = 1;   // TODO remove?
            log_inc = (double)2; // TODO remove?
        }

    N = stio_load_3yk (source1, source2, source3, &t, &y12, &y23, &y31);
    if (N < 1)
        {
            switch (N)
                {
                case 0:
                    printf ("# Empty file\n");
                    break;
                case -1:
                    printf ("# File not found\n");
                    break;
                case -2:
                    printf ("# Different file lengths\n");
                }
            exit (N);
        }
    // Optional drift removal
    if (dflag)
        {
            printf ("# Drift coefficients (y=a.t+b)\n");
            // Y12
            st_tchebyfit ((long)N, t, y12, &t_coeff);
            printf ("# %s: a=%24.16e \t b=%24.16e\n", source1, t_coeff.b, t_coeff.a);
            // Y23
            st_tchebyfit ((long)N, t, y23, &t_coeff);
            printf ("# %s: a=%24.16e \t b=%24.16e\n", source2, t_coeff.b, t_coeff.a);
            // Y31
            st_tchebyfit ((long)N, t, y31, &t_coeff);
            printf ("# %s: a=%24.16e \t b=%24.16e\n\n", source3, t_coeff.b, t_coeff.a);
        }

    // Generate Tau list
    serie[0].tau_base = serie[1].tau_base = serie[2].tau_base = *(t + 1) - *(t);
    for (k = 0; k < 3; ++k)
        {
            err = stu_populate_tau_list (&serie[k], N, flags.variance, OCT, NULL);
            if (err != 0)
                return -1;
        }

    // ADev of the measurement noise
    y = malloc (N * sizeof (double));
    for (i = 0; i < N; ++i)
        y[i] = (y12[i] + y23[i] + y31[i]) / sqrt ((double)3);
    flags.variance = AVAR;
    gadev[3].tau_base = *(t + 1) - *(t);
    err = stu_populate_tau_list (&gadev[3], N, flags.variance, OCT, NULL);
    if (err != 0)
        return -1;
    err = st_serie_dev (&gadev[3], flags.variance, N, y);
    if (err != 0)
        return -1;
    tarf[0] = fopen (target[3], "w");
    for (i = 0; i < gadev[3].length; ++i)
        {
            fprintf (tarf[0], "%12.6e %12.6e\n", gadev[3].tau[i], gadev[3].dev[i]);
            varnoise[i] = gadev[3].dev[i] * gadev[3].dev[i];
        }
    fclose (tarf[0]);
    flags.variance = fv_alt;

    // Dev of each file
    if (st_serie_dev (&serie[0], flags.variance, N, y12) != 0)
        return -1;
    if (st_serie_dev (&serie[1], flags.variance, N, y23) != 0)
        return -1;
    if (st_serie_dev (&serie[2], flags.variance, N, y31) != 0)
        return -1;

    printf ("Dev of each file\n");
    for (i = 0; i < serie[0].length; ++i)
        {
            printf ("%12.6e\t%12.6e\t%12.6e\t%12.6e\n", serie[0].tau[i], serie[0].dev[i], serie[1].dev[i], serie[2].dev[i]);
        }

    // GCoDev of each file pair or 3-cornered hat
    if (ind3ch) // Classical 3-cornered hat method
        for (i = 0; i < serie[0].length; ++i)
            {
                /* gcod are multiplied by -1 for the sake of compatibility
                   with the Groslambert covariance*/
                gadev[0].dev[i] = -(+serie[0].dev[i] - serie[1].dev[i] + serie[2].dev[i]) / ((double)2);
                gadev[1].dev[i] = -(+serie[0].dev[i] + serie[1].dev[i] - serie[2].dev[i]) / ((double)2);
                gadev[2].dev[i] = -(-serie[0].dev[i] + serie[1].dev[i] + serie[2].dev[i]) / ((double)2);

                printf ("Dev of gadev (Classic 3 cornered hat)\n");
            }
    else // Groslambert covariance method
        {
            flags.variance = GVAR;
            // Generate Tau list
            gadev[0].tau_base = gadev[1].tau_base = gadev[2].tau_base = *(t + 1) - *(t);
            for (k = 0; k < 3; ++k)
                {
                    err = stu_populate_tau_list (&gadev[k], N, flags.variance, OCT, NULL);
                    if (err != 0)
                        return -1;
                }
            if (st_serie_dev (&gadev[1], flags.variance, N, y12, y23) != 0)
                return -1;
            if (st_serie_dev (&gadev[2], flags.variance, N, y31, y23) != 0)
                return -1;
            if (st_serie_dev (&gadev[0], flags.variance, N, y31, y12) != 0)
                return -1;

            printf ("Dev of gadev (Grolambert)\n");
        }
    for (i = 0; i < serie[0].length; ++i)
        {
            printf ("%12.6e\t%12.6e\t%12.6e\t%12.6e\n", gadev[0].tau[i], gadev[0].dev[i], gadev[1].dev[i], gadev[2].dev[i]);
        }

    // Release memory allocated to the arrays y, y12, y23 and y31
    free (y);
    free (y12);
    free (y23);
    free (y31);

    // Estimation of the EDF
    flags.variance = fv_alt;
    
    printf("Estimation of the EDF\n");
    for (int k = 0; k < 3; ++k)
        {
            err = st_relatfit (&serie[k], serie[k].tau, 6, flags.slopes, flags.bias);
            for (i = 0; i < serie[k].length; ++i)
                {
                    asympt = 0;
                    for (j = 1; j < 5; ++j)
                        {
                            tsas = serie[k].asym[j] * st_interpo (serie[k].tau[i], j);
                            if (tsas > asympt)
                                {
                                    asympt = tsas;
                                    if (j == 1)
                        
                                        serie[k].alpha[i] = +2;
                                    else
                                        serie[k].alpha[i] = 2 - j;
                                }
                            printf("%12.6e\t%d\n", serie[k].asym[j], serie[k]. alpha[i]);
                        }
                }
            st_avardof (&serie[k], edf[k], flags.variance);
            printf("NEW I: %12.6e\t%d\t%12.6e\n", serie[k].asym[j], serie[k].alpha[i]), edf[k][i];
        }

    // EDF mean
    for (i = 0; i < serie[0].length; ++i)
        edfm[i] = (edf[0][i] + edf[1][i] + edf[2][i]) / ((double)3);

    // Computation of the GCod confidence intervals
    // Initialization
    nmont = (long)DATAMAX;
    for (k = 0; k < 3; ++k)
        {
            montecarl[k] = (double *)malloc (nmont * sizeof (double));
            pmont[k] = (double *)malloc (nmont * sizeof (double));
            asort[k] = (double *)malloc (nmont * sizeof (double));
            indices[k] = (long *)malloc (nmont * sizeof (long));
        }
    cd = (double *)malloc (nmont * sizeof (double));
    indix = (long *)malloc (nmont * sizeof (long));
    expal = (double *)malloc (nmont * sizeof (double));

    printf ("#  Tau \t B 2.5%%   < Clock 1  < B97.5%%   \t B 2.5%%   < Clock "
            "2  < B97.5%%   \t B 2.5%%   < Clock 3  < B97.5%%\n");
    for (k = 0; k < 3; ++k)
        {
            tarf[k] = fopen (target[0], "w");
            fprintf (tarf[k],
                     "# Tau        \t GCod         \t %s      \t B 2.5 %%     \t B 16 "
                     "%%       \t B 84 %%       \t B 97.5 %%\n",
                     chestim);
        }

    for (i = 0; i < serie[0].length; ++i)
        {
            // mesti[0] = -gadev[0].dev[i];
            // mesti[1] = -gadev[1].dev[i];
            // mesti[2] = -gadev[2].dev[i];
            for (j = 0; j < 3; ++j)
                {
                    mesti[j] = -gadev[j].dev[i];
                    if (mesti[j] < 0)
                        mesti[j] *= -mesti[j];
                    else
                        mesti[j] *= mesti[j];
                }
            if (edfm[i] < 100)
                klt = 1;
            else
                klt = 0;

            err = st_ci_kltgs (mesti, varnoise[i], edfm[i], klt, est, ci,
                               indices, montecarl, asort, pmont, cd, indix);
            for (j = 0; j < 3; ++j)
                {
                    for (k = 0; k < 3; ++k)
                        if (est[j][k] < 0)
                            est[j][k] = -sqrt (-est[j][k]);
                        else
                            est[j][k] = sqrt (est[j][k]);
                    for (k = 0; k < 5; ++k)
                        ci[j][k] = sqrt (ci[j][k]);
                    serie[j].conf_int[i].bmin2s = est[j][0];
                    if (est[j][1 + ind50] < bmin[j][i])
                        serie[j].conf_int[i].bmin2s = est[j][1 + ind50];
                    serie[j].conf_int[i].bmax2s = ci[j][4];
                }

            printf ("%6.0f \t %8.2e < %8.2e < %8.2e \t %8.2e < %8.2e < %8.2e "
                    "\t %8.2e < %8.2e < %8.2e\n",
                    serie[0].tau[i], ci[0][0], est[0][1 + ind50], ci[0][4], ci[1][0],
                    est[1][1 + ind50], ci[1][4], ci[2][0], est[2][1 + ind50],
                    ci[2][4]);
            fflush (stdout);

            for (k = 0; k < 3; ++k)
                {
                    fprintf (tarf[k],
                             "%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e \t "
                             "%12.6e \t %12.6e\n",
                             serie[k].tau[i], est[k][0], est[k][1 + ind50], ci[k][0], ci[k][1],
                             ci[k][2], ci[k][4]);
                    fflush (tarf[k]);
                }
            /*            fprintf (tarf1,
                                 "%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e \t "
                                 "%12.6e \t %12.6e\n",
                                 serie[0].tau[i], est[0][0], est[0][1 + ind50], ci[0][0], ci[0][1],
                                 ci[0][2], ci[0][4]);
                        fflush (tarf1);
                        fprintf (tarf2,
                                 "%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e \t "
                                 "%12.6e \t %12.6e\n",
                                 serie[1].tau[i], est[1][0], est[1][1 + ind50], ci[1][0], ci[1][1],
                                 ci[1][2], ci[1][4]);
                        fflush (tarf2);
                        fprintf (tarf3,
                                 "%12.6e \t %12.6e \t %12.6e \t %12.6e \t %12.6e \t "
                                 "%12.6e \t %12.6e\n",
                                 serie[02].tau[i], est[2][0], est[2][1 + ind50], ci[2][0], ci[2][1],
                                 ci[2][2], ci[2][4]);
                        fflush (tarf3);*/
        }
    fclose (tarf[0]);
    fclose (tarf[1]);
    fclose (tarf[2]);

    // Plot the graph(s)
    if (iflag)
        {
            for (k = 0; k < 3; ++k)
                {
                    // for(i=0;i<nto;++i) avar[i]=gadev[0].dev[i]*gadev[0].dev[i];
                    // err=st_relatfit(nto,tau,avar,tau,6);
                    err = st_relatfit (&gadev[0], gadev[0].tau, 6, flags.slopes, flags.bias);
                    err = gener_gplt (target[0], serie[k].length,
                                      serie[k].tau, serie[k].dev,
                                      serie[k].conf_int, chestim,
                                      a_coeff, flags);
                    if (err)
                        printf ("# Error %d: pdf file not created\n", err);
                }
        }
    else
        {
            for (k = 0; k < 3; ++k)
                {
                    for (i = 0; i < serie[k].length; ++i)
                        {
                            bmin[k][i] = serie[k].conf_int[i].bmin2s;
                            bmax[k][i] = serie[k].conf_int[i].bmax2s;
                        }
                }
            err = gen_3chplt (target, output, serie[0].length, serie[0].tau,
                              bmin, bmax, ind50, flags);
            if (err)
                printf ("# Error %d: pdf file not created\n", err);
        }

    // Release of the memory
    for (k = 0; k < 3; ++k)
        {
            free (montecarl[k]);
            free (pmont[k]);
            free (asort[k]);
            free (indices[k]);
        }
    free (cd);
    free (indix);
    free (expal);
    exit (err);
}
