/*   avardof.c                                     F. Vernotte - 2010/10/24 */
/*   Computation of the degrees of freedom of an Allan variance sequence    */
/*   following the paper "Uncertainty of Stability Variances Based on       */
/*   Finite Differences" by C. Greenhall and W. Riley (35th PTTI, San       */
/*   Diego, 2003, pp. 267-278)                                              */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "sigma_theta_cli.h"

#define db(x) ((double)(x))
#define sisig(x) ( (x) == 0 ) ? (db(0)) : (  ( (x) > 0 ) ? (db(1)) : (db(-1))  )

/**
 * \brief Help message
 */
void usage(void)
{
    printf("Usage: AVarDOF [-a|m|h|p] SOURCE\n\n");
    printf("Compute the degrees of freedom of the variance computations over a sequence of tau values.\n\n");
    printf("The input file SOURCE contains a 2-column table with tau values (integration time) in the first column and the exponent of the power law of the dominating noise in the second column.\n\n");
    printf("The first tau value is assumed to be equal to the sampling step.\n");
    printf("The last tau value is assumed to be equal to the half of the whole time sequence duration.\n\n");
    printf("A 2-column table containing tau values (integration time) in the first column and the equivalent degrees of freedom in the second column is sent to the standard output.\n\n");
    printf("If the option '-a' is selected, the variance is assumed to be the classical Allan variance (default).\n");
    printf("If the option '-m' is selected, the variance is assumed to be the modified Allan variance.\n");
    printf("If the option '-h' is selected, the variance is assumed to be the Hadamard variance.\n");
    printf("If the option '-p' is selected, the variance is assumed to be the parabolic variance.\n\n");
    printf("A redirection should be used for saving the results in a TARGET file: AVarDOF SOURCE > TARGET\n\n");
    printf("SigmaTheta %s %s - FEMTO-ST/OSU THETA/Universite de Franche-Comte/CNRS - FRANCE\n",st_version,st_date);
}


/**
 * \brief Computation of the degrees of freedom of the Allan variance
 *        estimates (based on "Uncertainty of stability variances based on
 *        finite differences" by C.A.Greenhall and W.J.Riley, 35th PTTI)
 * \param  2-column table containing the tau values and the dominant FM
 *         power law exponent for each tau value.
 * \return 2-column table containing the tau values and the equivalent
 *         degrees of freedom of each Allan variance measurement.
 */
int main(int argc, char *argv[])
{
    int i;
    double edf[32], alphadb[32];
    char gm[256], source[256], command[32];
    FILE *ofd;
    int retval=0;
    st_serie serie;
    flags_s flags;

    flags.variance=AVAR;
    if ((argc<2)||(argc>3))
        {
	usage();
	exit(-1);
        }
    else
	{
	if (argc==2)
		{
		strcpy(source,*++argv);
		flags.variance=AVAR;
		}
	else
		{
		strcpy(command,*++argv);
		if (command[0]=='-')
			{
			if ((!strcmp(command,"-a"))||(!strcmp(command,"-m"))||(!strcmp(command,"-h"))||(!strcmp(command,"-p"))) 
				{
				switch(command[1])
					{
					case 'p':
						flags.variance=PVAR;
						break;
					case 'h':
						flags.variance=HVAR;
						break;
					case 'm':
						flags.variance=MVAR;
						break;
					case 'a':
					default:
						flags.variance=AVAR;
					}
				strcpy(source,*++argv);
				}
			else
				{
				printf("Unknown option '%s'\n",command);
				usage();
				exit(-1);
				}
			}
		else
			{
			usage();
			exit(-1);
			}
		}
	}

    serie.length=stio_load_adev(source,serie.tau,alphadb);
    if (serie.length==-1)
        printf("# File not found\n");
    else
        {
            if (serie.length<2)
                {
                    printf("# Unrecognized file\n\n");
                    usage();
                }
            else
                {
                    for(i=0; i<serie.length; ++i) serie.alpha[i]=(int)alphadb[i];
                    if (flags.variance != PVAR)
						{
							if (st_avardof(&serie, edf, flags.variance)!=0)
								return -1;
						}
                    else
						{
							if (st_pvardof(&serie, edf)!=0)
								return -1;
						}
                    for(i=0;i<serie.length;++i)
                        printf("%24.16e \t %24.16e\n",serie.tau[i],edf[i]);
                }
	    }
    return 0;
}

