#   SIGMA-THETA Project - Makefile for the binaries of the SIGMA-THETA set:
#	    - 1col2col
#           - X2Y
#           - DriRem
#           - SigmaTheta
#           - ADev
#           - GCoDev
#	    - HCoDev
#           - MDev
#	    - HDev
#	    - PDev
#	    - Aver
#           - uncertainties
#           - RaylConfInt
#           - Asymptote
#	    - Asym2Alpha
#           - AVarDOF
#           - ADUncert
#           - ADGraph
#           - bruiteur
#
#                                                    SIGMA-THETA Project
#
# Copyright or © or Copr. Université de Franche-Comté, Besançon, France
# Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)
# Contact: francois.vernotte@obs-besancon.fr
#
# This software, SigmaTheta, is a collection of computer programs for
# time and frequency metrology.
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
#

# Uncomment one of the variable OPTIMIZE or DEBUG.
# -O3 (optimization) can cause strange behaviours, notably when debugging
#   changing -O3 to -Og should solve those issues
#OPTIMIZE = -O3
DEBUG = -g -Og

BINDIR = bin
OBJDIR = obj
SRCDIR = source
INSTALLDIR = /usr/bin
CONFDIR = /usr/share/sigmatheta

TARGETS = $(BINDIR)/1col2col $(BINDIR)/X2Y $(BINDIR)/DriRem  $(BINDIR)/ADev \
	$(BINDIR)/find_tau_xdev $(BINDIR)/GCoDev $(BINDIR)/MDev $(BINDIR)/HDev $(BINDIR)/HCoDev \
	$(BINDIR)/PDev $(BINDIR)/Aver $(BINDIR)/uncertainties $(BINDIR)/RaylConfInt \
	$(BINDIR)/Asymptote $(BINDIR)/Asym2Alpha $(BINDIR)/AVarDOF $(BINDIR)/ADUncert \
	$(BINDIR)/ADGraph $(BINDIR)/PSDGraph $(BINDIR)/YkGraph $(BINDIR)/XtGraph $(BINDIR)/DevGraph \
	$(BINDIR)/3CHGraph $(BINDIR)/bruiteur $(BINDIR)/GCUncert $(BINDIR)/3CorneredHat \
	$(BINDIR)/SigmaTheta

# ----------- Do not edit below -----------------------------------------------

# Optional SYMPREFIX can be set to have 'make install' build
# symlinks pointing to the SigmaTheta binaries with a prefix
# identification string ("st_" is a natural suggestion).
# This is a  convenient  way  to  have  shell  completion  show  all
# available SigmaTheta executables when working from command line
# by typing "st_" and tab twice.
#
# No symlink created if SYMPREFIX is empty.
#
SYMPREFIX = st_

MKDIR = mkdir -p
INSTALL = install
CC = gcc
GIT_VERSION := "$(shell git describe --abbrev=4 --dirty --always --tags)"
GIT_DATE := "$(shell git log | head -3 | grep Date | sed 's/Date:   //')"

# link statically against libfftw3
#FFTW3 = -static -lfftw3
# link dynamically against libfftw3
FFTW3 = -lfftw3
# link dynamically against libsigmatheta
SIGMATHETA = -lsigmatheta
SIGMATHETADIR = ./../libsigmatheta/build  # TMP: for development only!

CFLAGS = $(DEBUG) $(OPTIMIZE) -DST_VERSION=\"$(GIT_VERSION)\" -DST_DATE=\"$(GIT_DATE)\"
LDLIBS = -lm $(FFTW3) -L$(SIGMATHETADIR) $(SIGMATHETA)

#
# taking care of symlinks to be built or not :
#
ifneq ($(SYMPREFIX),)
SYMTARGETS := $(addprefix $(INSTALLDIR)$(SYMPREFIX), $(TARGETS))
endif

$(INSTALLDIR)$(SYMPREFIX)%: %
	ln -fs $(INSTALLDIR)$< $(INSTALLDIR)$(SYMPREFIX)$(<F)

.SECONDEXPANSION:

all: $(TARGETS)  | $$(@D)

clean:
	rm -f $(BINDIR)/*
	rm -f $(OBJDIR)/*.o


$(BINDIR)/1col2col : $(OBJDIR)/1col2col.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/X2Y : $(OBJDIR)/x2y.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/DriRem : $(OBJDIR)/drirem.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/SigmaTheta : $(OBJDIR)/SigmaTheta.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/ADev : $(OBJDIR)/adev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/find_tau_xdev : $(OBJDIR)/find_tau_xdev.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/GCoDev : $(OBJDIR)/gcodev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/MDev : $(OBJDIR)/mdev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/HDev : $(OBJDIR)/hdev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/HCoDev : $(OBJDIR)/hcodev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/PDev : $(OBJDIR)/pdev.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/Aver : $(OBJDIR)/aver.o
	$(CC) $(CFLAGS) -o $@ $^ -Wl,-Bdynamic -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/uncertainties : $(OBJDIR)/uncertainties.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/RaylConfInt : $(OBJDIR)/rayleigh.o
	$(CC) $(CFLAGS) -o $@ $^ -lgsl -lgslcblas -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/Asymptote : $(OBJDIR)/asymptote.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/Asym2Alpha : $(OBJDIR)/asym2alpha.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/AVarDOF : $(OBJDIR)/avardof.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/ADUncert : $(OBJDIR)/aduncert.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/ADGraph : $(OBJDIR)/adgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/PSDGraph : $(OBJDIR)/psdgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lgsl -lgslcblas -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/YkGraph : $(OBJDIR)/ykgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/XtGraph : $(OBJDIR)/xtgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/DevGraph : $(OBJDIR)/devgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/3CHGraph : $(OBJDIR)/3chgraph.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/bruiteur : $(OBJDIR)/bruiteur.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ $(FFTW3) -Wl,-Bdynamic -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/GCUncert : $(OBJDIR)/gcuncert.o $(OBJDIR)/flags.o
	$(CC) $(CFLAGS) -o $@ $^ $(FFTW3) -Wl,-Bdynamic -lgsl -lgslcblas -lm -L$(SIGMATHETADIR) $(SIGMATHETA)

$(BINDIR)/3CorneredHat : $(OBJDIR)/3_cornered_hat.o $(OBJDIR)/flags.o $(OBJDIR)/utils.o
	$(CC) $(CFLAGS) -o $@ $^ $(FFTW3) -Wl,-Bdynamic -lgsl -lgslcblas -lm -L$(SIGMATHETADIR) $(SIGMATHETA)


obj:
	@$(MKDIR) $@

$(OBJDIR)/utils.o : $(SRCDIR)/utils.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/1col2col.o : $(SRCDIR)/1col2col.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/x2y.o : $(SRCDIR)/x2y.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/flags.o : $(SRCDIR)/flags.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/drirem.o : $(SRCDIR)/drirem.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/SigmaTheta.o : $(SRCDIR)/SigmaTheta.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/adev.o : $(SRCDIR)/adev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/gcodev.o : $(SRCDIR)/gcodev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/mdev.o : $(SRCDIR)/mdev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/hdev.o : $(SRCDIR)/hdev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/hcodev.o : $(SRCDIR)/hcodev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/pdev.o : $(SRCDIR)/pdev.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/aver.o : $(SRCDIR)/aver.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/uncertainties.o : $(SRCDIR)/uncertainties.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/rayleigh.o : $(SRCDIR)/rayleigh.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/asymptote.o : $(SRCDIR)/asymptote.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/asym2alpha.o : $(SRCDIR)/asym2alpha.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/avardof.o : $(SRCDIR)/avardof.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/aduncert.o : $(SRCDIR)/aduncert.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/adgraph.o : $(SRCDIR)/adgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/psdgraph.o : $(SRCDIR)/psdgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/ykgraph.o : $(SRCDIR)/ykgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/xtgraph.o : $(SRCDIR)/xtgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/devgraph.o : $(SRCDIR)/devgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/3chgraph.o : $(SRCDIR)/3chgraph.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/bruiteur.o : $(SRCDIR)/bruiteur.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/gcuncert.o : $(SRCDIR)/gcuncert.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/3_cornered_hat.o : $(SRCDIR)/3_cornered_hat.c $(SRCDIR)/sigma_theta_cli.h | obj

$(OBJDIR)/ ziggurat.o : $(SRCDIR)/ziggurat.c $(SRCDIR)/zigtables.h | obj


man:
	$(MAKE) -C doc

maninstall:
	$(MAKE) -C doc install

confinstall: SigmaTheta.conf
	$(INSTALL) -D -m 644 SigmaTheta.conf --target-directory=$(CONFDIR)

baseinstall: $(TARGETS)
	$(INSTALL) -c -m 755 $(TARGETS) $(INSTALLDIR)

install: baseinstall $(SYMTARGETS) maninstall confinstall

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: all clean install


##install: $(TARGETS)
##	$(INSTALL) -c -m 755 $(TARGETS) $(INSTALLDIR)
